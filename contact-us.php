<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>Evault</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta
      name="description"
      content="Automation of task with minification of css and js"
    />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png" />

    <link href="assets/css/vendor.min.css" rel="stylesheet" />
    <link href="assets/css/styles.min.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.12/css/intlTelInput.min.css"
      integrity="sha512-yye/u0ehQsrVrfSd6biT17t39Rg9kNc+vENcCXZuMz2a+LWFGvXUnYuWUW6pbfYj1jcBb/C39UZw2ciQvwDDvg=="
      crossorigin="anonymous"
    />
  </head>
  <body class="NoOverflow rtl" id="body">
    <header id="header" class="headerspace NormalHeader">
      <div class="container">
        <div class="row">
          <div class="col-6 col-md-5">
            <div class="LogoBox">
              <a href="index.php" class="NormalLogo"
                ><img src="assets/img/logo.svg" alt="Evault"
              /></a>
              <!-- <a href="index.php" class="WhiteLogo"><img src="assets/img/white-logo.svg" alt="Evault"></a> -->
            </div>
          </div>
          <div class="col-6 col-md-7 TillIpad">
            <!-- <span class="menu" style="float: right"><a href="">Menu</a></span> -->
            <div
              class="menu"
            
            >
              <a class="btn-open" href="#">Menu</a>
            </div>
          </div>
          <div class="col-8 col-md-7 IpadProOnwards">
            <div class="NavigationBlock">
              <nav>
                <ul>
                  <!-- <li><a href="index.php">Home</a></li> -->
                  <li><a href="how-it-work.php">How It works</a></li>
                  <li><a href="gallery.php">Gallery</a></li>
                  <li><a href="faq.php">FAQ</a></li>
                  <li><a href="who-we-are.php">Who we are</a></li>
                  <li><a href="contact-us.php">Contact</a></li>
                  <!-- <li class="menu "><a href="#">Menu</a></li> -->
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div
      class="overlay"
      style="background: url(assets/img/menu-bg.png) no-repeat"
    >
      <div class="sub-menu">
        <div class="LogoNav">
          <a href="index.php" class="WhiteLogo"
            ><img src="assets/img/only-white.svg" alt="Evault"
          /></a>
          <span class="Menu"><a>Close</a></span>
        </div>
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="how-it-work.php">How it works</a></li>
          <li><a href="gallery.php">Gallery</a></li>
          <li><a href="faq.php">FAQ</a></li>
          <li><a href="who-we-are.php">Who we are</a></li>
          <li><a href="contact-us.php">Contact</a></li>
        </ul>
      </div>
    </div>

    <main style="min-height: 100vh">
      <section class="Section ContactUsSection">
        <div class="container">
          <div class="row">
            <div class="col-12 col-md-4">
              <div class="TopHeading">
                <h2 class="gs_reveal_heading medium-headings">Contact us</h2>
                <h4 class="GradientText">You ask, we answer.</h4>
              </div>
              <div class="AddressBlock Cards">
                <div class="Address Rollup">
                  <h4>
                    <a
                      href="https://www.google.com/maps/place/5th+Consulting+DMCC/@25.0692279,55.1403336,17z/data=!3m1!4b1!4m5!3m4!1s0x3e5f6d65f06c9c03:0x13dc0cfda9fda365!8m2!3d25.0692279!4d55.1425223"
                      target="blank"
                      ><img src="assets/img/add-icon.svg" alt="" /></a
                    >UAE
                  </h4>
                  <div>
                    <p>
                      RM 4204, Platinum Tower,<br />Cluster I, JLT, Dubai, UAE
                    </p>
                    <p>
                      <span>Phone: </span
                      ><a href="tel:+97142778085">+971 4 277 8085</a>
                    </p>
                    <p>
                      <span>Email: </span
                      ><a href="mailto:info@5th.ae">info@5th.ae</a>
                    </p>
                  </div>
                </div>
                <div class="Address Rollup">
                  <h4>
                    <a
                      href="https://www.google.com/maps/place/EMIRAT+AG/@48.1578031,11.5724886,17z/data=!3m1!4b1!4m8!1m2!2m1!1sEMIRAT+AG!3m4!1s0x479e7510943f79c5:0xaa7fe7363dba3626!8m2!3d48.1578031!4d11.5746773"
                      target="blank"
                      ><img src="assets/img/add-icon.svg" alt="" /></a
                    >Germany
                  </h4>
                  <div>
                    <p>Elisabethpl. 1, 80796 München,<br />Germany</p>
                    <p>
                      <span>Phone: </span
                      ><a href="tel:+49089255410">+49 (0) 89 255 41-0</a>
                    </p>
                    <p>
                      <span>Email: </span
                      ><a href="mailto:info@emirat.de">info@emirat.de</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-8 Cards">
              <div class="Details Rollup">
                <h3 class="gs_reveal_heading medium-headings">E-Vault</h3>
                <p>We look forward to hearing from you.</p>
                <!-- <p>Contact us info@5th.ae</p> -->
                <!-- <h5><a href="mailto:info@evault.com">info@evault.com</a></h5>
					<p>If you are interested in investing contact our investor relations team at:</p>
					<h5><a href="mailto:sales@evault.com">sales@evault.com</a></h5> -->
              </div>
              <div class="ContactForm Rollup">
                <form method="post" name="myForms" id="contact_form">
                  <label class="floating-label" for="">Name</label>
                  <input type="text" name="name" id="name" />
                  <small class="name-error"></small>
                  <label class="floating-label" for="">Email</label>
                  <input type="email" name="Email" id="email" />
                  <label class="floating-label" for="">Phone</label>
                  <input
                    type="tel"
                    name="Intl-Phone"
                    id="intl-phone"
                    required
                  />
                  <small class="phone-error"></small>
                  <label for="">Message</label>
                  <textarea name="Message" id="msg"></textarea>
                  <small class="msg-error"></small>
                  <p><input type="submit" class="submitBtn" value="Send" /></p>
                </form>
                <p class="error-message"></p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <?php @include('template-parts/footer.php') ?>
    </main>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.12/js/intlTelInput.js"
      integrity="sha512-lib7WdJvAnfLLBi5AHdN+wUkcWwpu1fmYzyEHQijZq7WedcwBiwdVNbcML2rAtKvwtOIU7thMGB+O9Kpb9g2Cw=="
      crossorigin="anonymous"
    ></script>
  </body>
</html>
