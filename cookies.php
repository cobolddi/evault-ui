<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>Evault</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta
      name="description"
      content="Automation of task with minification of css and js"
    />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png" />

    <link href="assets/css/vendor.min.css" rel="stylesheet" />
    <link href="assets/css/styles.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/PreloadJS/1.0.1/preloadjs.min.js"></script>
  </head>

  <body class="NoOverflow" id="body">
    <header id="header" class="headerspace NormalHeader">
      <div class="container">
        <div class="row">
          <div class="col-6 col-md-5">
            <div class="LogoBox">
              <a href="index.php" class="NormalLogo"
                ><img src="assets/img/logo.svg" alt="Evault"
              /></a>
              <!-- <a href="index.php" class="WhiteLogo"><img src="assets/img/white-logo.svg" alt="Evault"></a> -->
            </div>
          </div>
          <div class="col-6 col-md-7 TillIpad">
            <!-- <span class="menu" style="float: right"><a href="">Menu</a></span> -->
            <div
              class="menu"
              style="
                display: flex;
                align-items: center;
                justify-content: flex-end;
                font-weight: 600;
              "
            >
              <a class="btn-open" href="#">Menu</a>
            </div>
          </div>
          <div class="col-8 col-md-7 IpadProOnwards">
            <div class="NavigationBlock">
              <nav>
                <ul>
                  <!-- <li><a href="index.php">Home</a></li> -->
                  <li><a href="how-it-work.php">How it works</a></li>
                  <li><a href="gallery.php">Gallery</a></li>
                  <li><a href="faq.php">FAQ</a></li>
                  <li><a href="who-we-are.php">Who we are</a></li>
                  <li><a href="contact-us.php">Contact</a></li>
                  <!-- <li class="menu "><a href="#">Menu</a></li> -->
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div
      class="overlay"
      style="background: url(assets/img/menu-bg.png) no-repeat"
    >
      <div class="sub-menu">
        <div class="LogoNav">
          <a href="index.php" class="WhiteLogo"
            ><img src="assets/img/only-white.svg" alt="Evault"
          /></a>
          <span class="Menu"><a>Close</a></span>
        </div>
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="how-it-work.php">How it works</a></li>
          <li><a href="gallery.php">Gallery</a></li>
          <li><a href="faq.php">FAQ</a></li>
          <li><a href="who-we-are.php">Who we are</a></li>
          <li><a href="contact-us.php">Contact</a></li>
        </ul>
      </div>
    </div>

    <main>
      <section class="Section">
        <div class="container">
          <div class="general_content">
            <h2>Cookies</h2>
            <p>
              This website uses cookies. We use cookies to personalise content
              and ads, to provide social media features and to analyse our
              traffic. We also share information about your use of our site with
              our social media, advertising and analytics partners who may
              combine it with other information that you’ve provided to them or
              that they’ve collected from your use of their services.
            </p>

            <p>
              Cookies are small text files that can be used by websites to make
              a user's experience more efficient.
            </p>

            <p>
              The law states that we can store cookies on your device if they
              are strictly necessary for the operation of this site. For all
              other types of cookies we need your permission.
            </p>
            <p>
              This site uses different types of cookies. Some cookies are placed
              by third party services that appear on our pages.
            </p>
            <p>
              You can at any time change or withdraw your consent from the
              Cookie Declaration on our website.
            </p>
          </div>
        </div>
      </section>
    </main>

    <?php @include('template-parts/footer.php') ?>
  </body>
</html>
