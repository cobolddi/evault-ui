<?php @include('template-parts/header.php') ?>

<!-- CSS at home.scss -->
<div class="videoSection-videoPage js-videoWrapper">
  <video
    src="assets/img/new-video/video-3.mp4"
    poster="assets/img/evault/Home/video-bg.jpg"
    controls
    class="js-videoIframe"
  ></video>
  <button class="videoPoster js-videoPoster">
    <img src="assets/img/play.svg" alt="" class="play" />
  </button>
</div>

<?php @include('template-parts/footer.php') ?>
