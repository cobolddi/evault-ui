<?php
/**
 * Template Name: Home Page
 */

get_header();
get_template_part('parts/home/banner');
get_template_part('parts/home/introducing');
get_template_part('parts/home/riskreward');
get_template_part('parts/home/freeparticipate');
get_template_part('parts/home/threecards');
get_template_part('parts/home/fivecards');
get_template_part('parts/home/videoplaysection');
get_template_part('parts/home/logotestimonials');
get_footer();
