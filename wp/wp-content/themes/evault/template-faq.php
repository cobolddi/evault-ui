<?php
/**
 * Template Name: FAQ page
 */

get_header();
?>
  <section class="FaqSection">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-4 faqSectionHeading">
          <h2 class="gs_reveal_heading large-headings mb1-6"><?php the_field('heading') ?></h2>
          <h4 class="GradientText"><?php the_field('sub_heading'); ?></h4>
        </div>
        <div class="col-12 col-md-8">
          <div class="accordionWrapper Cards">
            <?php 
              $x = 0;
              while(have_rows('faqs')): $x++; the_row(); ?>
              <div class="accordionItem <?php if($x==1) echo 'open'; ?> Rollup">
                <h5 class="accordionItemHeading">
                  <span class="question"><?php the_sub_field('question') ?></span>
                  <span class="plusIcon"></span>
                </h5>
                <div class="accordionItemContent">
                  <p><?php the_sub_field('answer'); ?></p>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script>
    var x = document.getElementById("header");
    x.classList.add("NormalHeader");
  </script>
<?php
get_footer();
