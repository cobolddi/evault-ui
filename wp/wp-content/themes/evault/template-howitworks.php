<?php
/**
 * Template Name: How it works page
 */

get_header();
get_template_part('parts/howitworks/banner');
get_template_part('parts/howitworks/centercontent');
get_template_part('parts/howitworks/imagecards');
get_template_part('parts/howitworks/premium');
get_template_part('parts/howitworks/cta');

get_footer();
