<?php
/**
 * Template Name: Contact us page
 */

get_header();
?>
  <section class="Section ContactUsSection">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-4">
          <div class="TopHeading">
            <h2 class="gs_reveal_heading medium-headings"><?php the_field('heading'); ?></h2>
            <h4 class="GradientText"><?php the_field('sub_heading'); ?></h4>
          </div>
          <div class="AddressBlock Cards">
            <?php while(have_rows('addresses')): the_row(); ?>
              <div class="Address Rollup">
                <h4>
                  <a
                    href="<?php the_sub_field('link'); ?>"
                    target="blank"
                    ><img src="<?php echo get_template_directory_uri() ?>/assets/img/add-icon.svg" alt="" /></a
                  ><?php the_sub_field('country'); ?>
                </h4>
                <div>
                    <?php the_sub_field('address'); ?>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        </div>
        <div class="col-12 col-md-8 Cards">
          <div class="Details Rollup">
            <h3 class="gs_reveal_heading medium-headings"><?php the_field('contact_form_heading') ?></h3>
            <p><?php the_field('contact_form_description'); ?></p>
          </div>
          <div class="ContactForm Rollup" style="margin-top: 3em;">
            <?php echo do_shortcode(get_field('contact_form','option')); ?>
            <!-- <form method="post" name="myForms" id="contact_form">
              <label class="floating-label" for="">Name</label>
              <input type="text" name="name" id="name" />
              <small class="name-error"></small>
              <label class="floating-label" for="">Email</label>
              <input type="email" name="Email" id="email" />
              <label class="floating-label" for="">Phone</label>
              <input
                type="tel"
                name="Intl-Phone"
                id="intl-phone"
                required
              />
              <small class="phone-error"></small>
              <label for="">Message</label>
              <textarea name="Message" id="msg"></textarea>
              <small class="msg-error"></small>
              <p><input type="submit" class="submitBtn" value="Send" /></p>
            </form>
            <p class="error-message"></p> -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <script
      src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.12/js/intlTelInput.js"
      integrity="sha512-lib7WdJvAnfLLBi5AHdN+wUkcWwpu1fmYzyEHQijZq7WedcwBiwdVNbcML2rAtKvwtOIU7thMGB+O9Kpb9g2Cw=="
      crossorigin="anonymous"
    ></script>
  <script>
    var x = document.getElementById("header");
    x.classList.add("NormalHeader");
  </script>
<?php
get_footer();
