<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package evault
 */
$c = get_field('footer', 'option');
?>
	</main>
	<footer>
		<div class="container">
			<div class="Logoblock">
				<a href="<?php echo bloginfo('url'); ?>">
					<img src="<?php $x=get_field('logo','option'); echo $x['logo_white']['url'] ?>" alt="EVAULT" style="width:130px">
				</a>
			</div>
			<div class="AddressWithSocial">
				<div class="AddressBlock">
					<?php foreach($c['addresses'] as $address): ?>
						<div class="Address">
								<h4><a href="<?php echo $address['link'] ?>" target="blank"><img src="<?php echo get_template_directory_uri() ?>/assets/img/add-icon.svg" alt=""></a><?php echo $address['country'] ?></h4>
								<p><?php echo $address['address']; ?></p>
								<p><a href="mailto:<?php echo $address['email'] ?>"><?php echo $address['email'] ?></a></p>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="SocialBlock">
					<ul>
						<li><a href="<?php echo $c['facebook_link']; ?>" target=_blank><img src="<?php echo get_template_directory_uri() ?>/assets/img/fb-icon.svg" alt=""></a></li>
						<li><a href="<?php echo $c['instagram_link'] ?>" target=_blank><img src="<?php echo get_template_directory_uri() ?>/assets/img/insta-icon.svg" alt=""></a></li>
						<li><a href="<?php echo $c['linkedin_link']; ?>" target=_blank><img src="<?php echo get_template_directory_uri() ?>/assets/img/linkedin-icon.svg" alt=""></a></li>
					</ul>
				</div>
			</div>
			<div class="CopyrightWithLinks">
				<div class="Copyright">
					<p><?php echo $c['copyright_notice']; ?></p>
				</div>
				<div class="QuickLink">
					<ul>
						<li><a href="<?php echo bloginfo('url') ?>/cookies"><?php pll_e('Cookies') ?></a></li>
						<li><a href="<?php echo bloginfo('url') ?>/privacy-policy"><?php pll_e('Privacy Policy') ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

	<script src="<?php echo get_template_directory_uri() ?>/assets/js/vendor.min.js"></script>
	<script src="<?php echo get_template_directory_uri() ?>/assets/js/scripts.min.js"></script>
	<style>
		.ContactUsSection .ContactForm input[type=email].wpcf7-not-valid, .ContactUsSection .ContactForm input[type=tel].wpcf7-not-valid, .ContactUsSection .ContactForm input[type=text].wpcf7-not-valid, .ContactUsSection .ContactForm textarea.wpcf7-not-valid  {
			margin-bottom:  1em;
		}
		.wpcf7-not-valid-tip {
			margin-bottom:  1em;
		}
		.wpcf7-form.invalid .iti {
			margin-bottom:  1em !important;
		}
		.wpcf7-form.failed .wpcf7-response-output {
			background: #fbf0f0;
			color:  #990000;
			border:  none;
			padding:  12px;
			border-radius: 4px;
			margin: .5em 0;
		}
		.wpcf7-form.sent .wpcf7-response-output {
			background: #ddf0dd;
			color:  #009900;
			padding:  12px;
			border-radius: 4px;
			margin: .5em 0;
			border:  none;
		}
	</style>
<?php wp_footer(); ?>

</body>
</html>
