<?php
/**
 * Template Name: Gallery page
 */

get_header();
?>
  <section class="Gallery">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-4 galleryHeading">
          <h2 class="gs_reveal_heading large-headings mb1-6">
            <?php the_field('heading') ?>            
          </h2>
          <h4 class="GradientText mb1-6">
            <?php the_field('sub_heading') ?>            
          </h4>
          <p>
            <?php the_field('description') ?>            
          </p>
        </div>
        <div class="col-12 col-md-8">
          <div class="GalleryTabs">
            <div class="buttons">
              <a class="tab-links active" onclick="openTab(event, 'first')"
                ><?php pll_e('All'); ?></a
              >
              <a class="tab-links" onclick="openTab(event, 'second')"
                ><?php pll_e('Photos'); ?></a
              >
              <a class="tab-links" onclick="openTab(event, 'third')"
                ><?php pll_e('Videos'); ?></a
              >
            </div>

            <div id="first" class="tab-content">
              <div class="flexbox" id="gallery">
                <?php 
                  $photos = get_field('photos');
                  foreach($photos as $image): ?>
                  <a
                    href="<?php echo $image['url'] ?>"
                    class="item image-popup-vertical-fit"
                    ><img src="<?php echo $image['sizes']['medium'] ?>" alt=""
                  /></a>
                <?php endforeach; ?>

                <?php 
                  $video_posters = get_field('video_posters');
                  $id = 0;
                  foreach($video_posters as $image): $id++ ?>
                  <a href="#video-<?php echo $id ?>" class="openVideo item">
                    <img src="<?php echo $image['sizes']['medium'] ?>" alt="" />
                  </a>
                <?php endforeach; ?>
              </div>
            </div>

            <div id="second" class="tab-content">
              <div class="flexbox" id="gallery">
                <?php 
                  $photos = get_field('photos');
                  foreach($photos as $image): ?>
                  <a
                    href="<?php echo $image['url'] ?>"
                    class="item image-popup-vertical-fit"
                    ><img src="<?php echo $image['sizes']['medium'] ?>" alt=""
                  /></a>
                <?php endforeach; ?>
              </div>
            </div>

            <div id="third" class="tab-content">
              <div class="flexbox" id="gallery">
                <?php 
                  $video_posters = get_field('video_posters');
                  $id = 0;
                  foreach($video_posters as $image): $id++ ?>
                  <a href="#video-<?php echo $id ?>" class="openVideo item">
                    <img src="<?php echo $image['sizes']['medium'] ?>" alt="" />
                  </a>
                <?php endforeach; ?>
                <?php 
                  $videos = get_field('videos');
                  $id = 0;
                  foreach($videos as $video): $id++ ?>
                    <div id="video-<?php echo $id ?>" class="video-popup mfp-hide">
                      <video
                        preload="false"
                      >
                        <source
                          src="<?php echo $video['url'] ?>"
                          type="video/mp4"
                        />
                      </video>
                    </div>
                  <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script>
    var x = document.getElementById("header");
    x.classList.add("NormalHeader");
  </script>
<?php
get_footer();
