<?php
/**
 * Template Name: Who we are page
 */

get_header();
get_template_part('parts/whoweare/banner');
get_template_part('parts/whoweare/threecards');
get_template_part('parts/whoweare/team');
get_footer();
