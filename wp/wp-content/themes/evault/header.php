<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package evault
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="<?php echo get_template_directory_uri(); ?>/assets/css/vendor.min.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/assets/css/styles.min.css" rel="stylesheet">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/PreloadJS/1.0.1/preloadjs.min.js"></script>
	<link
	  rel="stylesheet"
	  href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.12/css/intlTelInput.min.css"
	  integrity="sha512-yye/u0ehQsrVrfSd6biT17t39Rg9kNc+vENcCXZuMz2a+LWFGvXUnYuWUW6pbfYj1jcBb/C39UZw2ciQvwDDvg=="
	  crossorigin="anonymous"
	/>
	<?php wp_head(); ?>
	<style>
		a.skip-link {
			display:  none;
		}
		.MenuCloser a { 
			color: #fff; 
			font-family:  Montserrat,sans-serif;
		}
		@media only screen and (max-width: 767px) {
		    footer .CopyrightWithLinks {
		        display: block;
		    }
		    footer .CopyrightWithLinks .QuickLink { 
		        justify-content:  flex-start;
		    }
		}
	</style>
	<?php 
		if(get_locale() == 'ar_AE') {
			?>
			<link href="<?php echo get_template_directory_uri(); ?>/arabic-font/stylesheet.css" rel="stylesheet" />
			<?php
		} 
	?>
</head>

<body id="body">
	<div id="preloader">
		<div id="status">
			<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
				<circle class="circleSvg" cx="50" cy="50" r="45" />
			</svg>
		</div>
	</div>

	<script>
		document.addEventListener("DOMContentLoaded", init);
		let queue = new createjs.LoadQueue();
		const preload = document.querySelector('#preloader');
		const circle = preload.querySelector(".circleSvg")
		function init() {

			queue.on("progress", handleFileProgress);
			queue.on("fileload", handleFileLoad);
			queue.on("complete", handleComplete);

			queue.loadManifest([
				{ id: "video-1", src: './assets/img/new-video/video-1.mp4' },
				{ id: "image-1", src: './assets/img/evault/Home/1.jpg' },
				{ id: "image-2", src: './assets/img/evault/Home/2.jpg' },
				{ id: "image-3", src: './assets/img/evault/Home/3.jpg' }
			])
		}

		function handleFileProgress(e) {
			circle.style.strokeDashoffset = `${280 - (queue.progress * 280)}px`

		}
		function handleFileLoad(e) {
			const itemId = queue.getResult(e)

		}

		function handleComplete(e) {
			preload.style.transform = 'translateY(-100vh)';
			checkLoadedFunction()
		}

	</script>

	<?php wp_body_open(); ?>
		<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'evault' ); ?></a>

		<header id="header" class="headerspace">
			<div class="container">
				<div class="row">
					<div class="col-6 col-md-5">
						<div class="LogoBox">
							<a href="<?php echo bloginfo('url'); ?>" class="NormalLogo">
								<img src="<?php $x=get_field('logo','option'); echo $x['logo_color']['url'] ?>" alt="EVAULT" style="width:130px">
							</a>
						</div>
					</div>
					<div class="col-6 col-md-7 TillIpad">
						<div class="menuToggler" style="display: flex; align-items: center; justify-content: flex-end; font-weight: 600;">
							<a class="btn-open" href="#"><?php echo pll_e('Menu'); ?></a>
						</div>
					</div>
					<div class="col-8 col-md-7 IpadProOnwards">
						<div class="NavigationBlock">
							<nav>
								<?php wp_nav_menu('menu-1') ?>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div class="overlay" style="background:#1C252F url(<?php echo get_template_directory_uri() ?>/assets/img/menu-bg.png) no-repeat;">
			<div class="sub-menu">
				<div class="LogoNav">
					<a href="index.php" class="WhiteLogo"><img src="<?php echo get_template_directory_uri() ?>/assets/img/only-white.svg" alt="Evault"></a>
					<span class="MenuCloser"><a><?php echo pll_e('Close'); ?></a></span>
				</div>
				<ul>
					<?php wp_nav_menu('menu-1') ?>
				</ul>
			</div>
		</div>

	<main>

