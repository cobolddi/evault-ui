<?php $c = get_field('free_participate'); ?>
<section class="HomeBanner Equal">
		<div class="Video">
			<img src="<?php echo get_template_directory_uri() ?>/assets/img/evault/Home/3.jpg" />
		</div>
		<div class="BannerContent">
			<div class="BannerText">
				<div class="homeBanner">
					<div class="delay_15">
                    <div class="Content">
						<h2 class="large-headings mb1-6">
					<span class="gs_reveal_heading" style="display: block;"><?php echo $c['heading_1'] ?></span>
					<span class="gs_reveal_heading" style="display: block;"><?php echo $c['heading_2'] ?></span></h2>
						<h4 class=" GradientText gs_fade_reveal"><?php echo $c['sub_heading'] ?></h4>
						<p class="gs_fade_reveal"><?php echo $c['description'] ?></p>
						<ul class="lists">
							<?php foreach($c['list_items'] as $item): ?>
								<li class="bulletPoints gradBul-js"><span><?php echo $item['item'] ?></span></li>
							<?php endforeach; ?>
						</ul>
					</div>	
				</div>
			</div>
		</div>
</section>