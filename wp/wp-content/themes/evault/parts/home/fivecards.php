<?php $c = get_field('five_cards'); ?>
<section class="BgVideoWithFiveCards darkBg">
  <video
    src="<?php echo $c['video']['url'] ?>"
    autoplay
    loop
    muted
    playsinline
  ></video>
  <div class="container" style="z-index: 2">
    <div class="TopHeading">
      <h2 class="gs_reveal_heading large-headings">
        <?php echo $c['heading'] ?>
      </h2>
    </div>
    <div class="FiveCardsBlock">
      <div class="TwoCardsRow">
        <div class="Cards">
          <h3 class="Rollup">
            <?php echo $c['cards'][0]['heading'] ?>
          </h3>
          <p class="Rollup">
            <?php echo $c['cards'][0]['description'] ?>
          </p>
        </div>
        <div class="Cards">
          <h3 class="Rollup">
            <?php echo $c['cards'][1]['heading'] ?>
          </h3>
          <p class="Rollup">
            <?php echo $c['cards'][1]['description'] ?>
          </p>
        </div>
      </div>
      <div class="TwoCardsRow">
        <div class="Cards">
          <h3 class="Rollup">
            <?php echo $c['cards'][2]['heading'] ?>
          </h3>
          <p class="Rollup">
            <?php echo $c['cards'][2]['description'] ?>
          </p>
        </div>
        <div class="Cards">
          <h3 class="Rollup">
            <?php echo $c['cards'][3]['heading'] ?>
          </h3>
          <p class="Rollup">
            <?php echo $c['cards'][3]['description'] ?>
          </p>
        </div>
      </div>
      <div class="TwoCardsRow SingleCard">
        <div class="Cards">
          <h3 class="Rollup">
            <?php echo $c['cards'][4]['heading'] ?>
          </h3>
          <p class="Rollup">
            <?php echo $c['cards'][4]['description'] ?>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
