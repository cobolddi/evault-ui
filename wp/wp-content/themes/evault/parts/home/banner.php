<?php 
  $c = get_field('home_banner');
?>
<section class="HomeBanner">
  <div class="BannerContent">
    <div class="BannerText">
      <div class="homeBanner">
        <div class="delay_15">
          <h1 class="gs_reveal_heading large-headings hero_title home_hero_title" id="title1">
            <?php echo $c['heading']; ?>
          </h1>
          <h4 class="gs_reveal GradientText">
            <?php echo $c['sub_heading']; ?>
          </h4>
          <p class="gs_reveal">
            <?php echo $c['description']; ?>
          </p>
        </div>
      </div>
    </div>
    <div class="LearnMore">
      <a href="#second-fold" class=""><?php echo $c['learn_more']; ?></a>
    </div>
  </div>
  <div class="Video">
    <video
      src="<?php echo get_template_directory_uri() ?>/assets/img/new-video/video-1.mp4"
      loop
      autoplay
      muted
      playsinline
    ></video>
  </div>
</section>