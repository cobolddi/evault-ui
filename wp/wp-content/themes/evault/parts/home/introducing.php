<?php 
	$c = get_field('introducing');
?>
<section id="second-fold" class="RightContentWithBg darkBg" id="VideoWithContent">
	<div class="RightContentbox">
		<h2 class="gs_reveal_heading large-headings"><?php echo $c['heading'] ?></h2>
		<h4 class=" GradientText gs_fade_reveal"><?php echo $c['sub_heading']; ?></h4>
		<p class="gs_fade_reveal"><?php echo $c['description'] ?></p>
	</div>
</section>