<?php $c = get_field('logo_testimonials'); ?>
<section
  class="Section LogotestimonialBlock testimonial"
  data-section="whiteBody"
>
  <div class="container">
    <div class="TopHeading">
      <h2 class="gs_reveal_heading large-headings"><?php echo $c['heading']; ?></h2>
    </div>
    <div class="DualSliderBlock">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="logoslider">
            <div class="fourlogoslider">
              <div class="FourLogos">
                <div class="Animateblock">
                  <?php foreach($c['logos'] as $logo): ?>
                    <div class="logo">
                      <img src="<?php echo $logo['url'] ?>" alt="" />
                    </div>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6">
          <div class="TestimonialBlock">
            <div class="testimonialslider">
              <?php foreach($c['testimonials'] as $t): ?>
                <div class="testimonial">
                  <h4 class="GradientText slideDescription">
                   <?php echo $t['heading'] ?>
                  </h4>
                  <p class="slideSubtitle">
                   <?php echo $t['description']; ?>
                  </p>   
                </div>
              <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="TopHeading CTAblock">
      <h2 class="gs_reveal_heading large-headings">
        <?php echo $c['cta_heading']; ?>
      </h2>
      <a href="<?php echo $c['cta_button']['url'] ?>" class="GreyBtn"
        ><?php echo $c['cta_button']['title'] ?><img src="<?php echo get_template_directory_uri() ?>/assets/img/arw-right.svg" alt=""
      /></a>
    </div>
  </div>
</section>
