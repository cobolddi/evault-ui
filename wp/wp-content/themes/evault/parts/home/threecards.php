<?php $c = get_field('three_cards'); ?>
<section class="ThreeCardsBlock whiteBg" data-section="whiteBody">
  <?php foreach($c['image_1'] as $card) : ?>
    <div class="OnhoverVideoBlock">
      <video width="100%" height="100%" loop preload="none" muted playsinline>
        <source src="<?php echo get_template_directory_uri() ?>/assets/img/new-video/new-bg-video-3.mp4" type="video/mp4" />
      </video>
      <div class="IconWithHeading">
        <img src="<?php echo $card['icon']['url'] ?>" alt="" />
        <h2 class="videocontent"><?php echo $card['heading'] ?></h2>
        <p class="videocontent">
          <?php echo $card['description'] ?>
        </p>
      </div>
    </div>
  <?php endforeach; ?>
</section>
