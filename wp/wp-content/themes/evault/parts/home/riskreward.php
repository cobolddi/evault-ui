<?php $c=get_field('risk_reward'); ?>
<section class="HomeBanner Reverse">
		<div class="Video">
			<img src="<?php echo get_template_directory_uri() ?>/assets/img/evault/Home/2.jpg" />
		</div>
		<div class="BannerContent">
			<div class="BannerText">
				<div class="homeBanner">
					<div class="delay_15">
                    <h2 class="gs_reveal_heading large-headings"><?php echo $c['heading'] ?></h2>
						<h4 class="GradientText gs_fade_reveal"><?php echo $c['sub_heading'] ?></h4>
						<p class="gs_fade_reveal"><?php echo $c['description'] ?></p>
					</div>	
				</div>
			</div>
		</div>
</section>