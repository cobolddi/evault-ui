<?php $c = get_field('video_section'); ?>
<section class="WorkWellVideo videoBg" style="background: #000" id="video" >
  <div
    class="VideoContainer js-videoWrapper"
    data-aos="zoom-in"
  >
  <?php 
    if(get_locale() == 'ar_AE') {
      ?>
      <video
        src="<?php echo get_template_directory_uri() ?>/assets/img/new-video/evault_2021_AR.mp4"
        poster="<?php echo $c['poster']['url']; ?>"
        class="videoIframe js-videoIframe"
      ></video>
      <?php
    } else {
      ?>
      <video
        src="<?php echo get_template_directory_uri() ?>/assets/img/new-video/video-3.mp4"
        poster="<?php echo $c['poster']['url']; ?>"
        class="videoIframe js-videoIframe"
      ></video>
      <?php
    }
  ?>
    
    <button class="videoPoster js-videoPoster">
      <img src="<?php echo get_template_directory_uri() ?>/assets/img/play.svg" alt="" class="play" />
    </button>
  </div>
</section>
