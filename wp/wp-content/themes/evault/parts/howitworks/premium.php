<?php $c = get_field('premium'); ?>
<section class="CenterContentOnly WithThreeCards">
	<div class="container">
		<div class="CenterContent">
			<h2 class="gs_reveal_heading large-headings"><?php echo $c['heading'] ?></h2>
			<h4 class="gs_fade_reveal GradientText"><?php echo $c['sub_heading'] ?></h4>
			<p class="gs_fade_reveal"><?php echo $c['description'] ?></p>
		</div>
		<div class="ThreeCardsWrapper Cards">
			<ul>
				<li>
					<h5 class="Rollup"><?php echo $c['cards'][0]['heading'] ?></h5>
					<p class="Rollup"><?php echo $c['cards'][0]['description'] ?></p>
				</li>
				<li>
					<h5 class="Rollup"><?php echo $c['cards'][1]['heading'] ?></h5>
					<p class="Rollup"><?php echo $c['cards'][1]['description'] ?></p>
				</li>
				<li>
					<h5 class="Rollup"><?php echo $c['cards'][2]['heading'] ?></h5>
					<p class="Rollup"><?php echo $c['cards'][2]['description'] ?></p>
				</li>
			</ul>
		</div>
	</div>
</section>