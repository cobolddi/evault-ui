<?php $c = get_field('center_content'); ?>
<section class="CenterContentOnly">
	<div class="container">
		<div class="CenterContent">
			<h2 class="gs_reveal_heading large-headings"><?php echo $c['heading']; ?></h2>
			<p class="gs_fade_reveal"><?php echo $c['description']; ?></p>
		</div>
	</div>
</section>