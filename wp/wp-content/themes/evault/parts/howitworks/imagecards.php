<?php $c = get_field('image_cards'); ?>
<section class="left_image_right_content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 MobileOnly">
        <div class="imgWrap">
          <img src="<?php echo $c['cards'][0]['image']['url'] ?>" alt="" />
        </div>
      </div>

      <div class="col-md-6">
        <div class="contentWrap text-left-md">
          <h2 class="gs_reveal_heading">
            <?php echo $c['cards'][0]['heading'] ?>
          </h2>
          <p class="gs_fade_reveal">
            <?php echo $c['cards'][0]['description'] ?>
          </p>
        </div>
      </div>

      <div class="col-md-6 DesktopOnly">
        <div class="imgWrap">
          <img src="<?php echo $c['cards'][0]['image']['url'] ?>" alt="" />
        </div>
      </div>

      <div class="col-md-6">
        <div class="imgWrap">
          <img src="<?php echo $c['cards'][1]['image']['url'] ?>" alt="" />
        </div>
      </div>
      <div class="col-md-6">
        <div class="contentWrap mr-5">
          <h2 class="gs_reveal_heading">
            <?php echo $c['cards'][1]['heading'] ?>
          </h2>
          <p class="gs_fade_reveal">
            <?php echo $c['cards'][1]['description'] ?>
          </p>
        </div>
      </div>

      <div class="col-md-6 MobileOnly">
        <div class="imgWrap">
          <img src="<?php echo $c['cards'][2]['image']['url'] ?>" alt="" />
        </div>
      </div>

      <div class="col-md-6">
        <div class="contentWrap text-left-md">
          <h2 class="gs_reveal_heading">
            <?php echo $c['cards'][2]['heading'] ?>
          </h2>
          <p class="gs_fade_reveal">
            <?php echo $c['cards'][2]['description'] ?>
          </p>
        </div>
      </div>

      <div class="col-md-6 DesktopOnly">
        <div class="imgWrap">
          <img src="<?php echo $c['cards'][2]['image']['url'] ?>" alt="" />
        </div>
      </div>
    </div>
  </div>
</section>