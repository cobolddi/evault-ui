<?php $c=get_field('cta'); ?>
<section class="HeadingWithLink CTAblock">
  <div class="container">
    <h2 class="gs_reveal_heading large-headings">
      <?php echo $c['heading'] ?>
    </h2>
    <a href="<?php echo $c['button']['url'] ?>" class="GreyBtn"
      ><?php echo $c['button']['title'] ?> <img src="<?php echo get_template_directory_uri() ?>/assets/img/arw-right.svg" alt=""
    /></a>
  </div>
</section>