<?php $c = get_field('banner'); ?>
<section class="HomeBanner">
  <div class="BannerContent">
    <div class="BannerText">
      <div class="homeBanner">
        <div class="delay_15">
          <h2 class="gs_reveal_heading large-headings mb1-6">
            <?php echo $c['heading'] ?>
          </h2>
          <h4 class="GradientText gs_reveal">
            <?php echo $c['sub_heading'] ?>
          </h4>
          <p class="gs_reveal">
            <?php echo $c['description']; ?>
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="Video">
    <img
      src="<?php echo $c['image']['url'] ?>"
      alt=""
      style="width: 100%; display: block;"
    />
  </div>
</section>