<?php $c = get_field('three_cards'); ?>
<section class="CenterTopIconBtmContent ThreeCardsBlock">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-4">
				<div class="TopIconBtmContent">
					<img src="<?php echo $c['cards'][0]['icon']['url'] ?>" alt="" class="videocontent">
					<h4 class="videocontent gs_reveal_heading"><?php echo $c['cards'][0]['heading'] ?></h4>
					<p class="videocontent gs_fade_reveal"><?php echo $c['cards'][0]['description'] ?></p>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="TopIconBtmContent">
					<img src="<?php echo $c['cards'][1]['icon']['url'] ?>" alt="" class="videocontent">
					<h4 class="videocontent gs_reveal_heading"><?php echo $c['cards'][1]['heading'] ?></h4>
					<p class="videocontent gs_fade_reveal"><?php echo $c['cards'][1]['description'] ?></p>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="TopIconBtmContent">
					<img src="<?php echo $c['cards'][2]['icon']['url'] ?>" alt="" class="videocontent">
					<h4 class="videocontent gs_reveal_heading"><?php echo $c['cards'][2]['heading'] ?></h4>
					<p class="videocontent gs_fade_reveal"><?php echo $c['cards'][2]['description'] ?></p>
				</div>
			</div>
		</div>
	</div>
</section>