<section class="TeamBlock">
  <div class="container">
    <div class="TopHeading">
      <h2 class="gs_reveal_heading large-headings"><?php the_field('team_heading') ?></h2>
    </div>
    <div class="TeamWrapper ThreeCardsBlock">
      <?php while(have_rows('team')): the_row(); ?>
        <div class="TeamMember">
          <img src="<?php $x = get_sub_field('photo'); echo $x['url'] ?>" alt="" />
          <div class="MemeberDetails">
            <h4 class="teamContent"><?php the_sub_field('name'); ?></h4>
            <h5 class="teamContent GradientText"><?php the_sub_field('designation'); ?></h5>
            <p class="teamContent">
              <?php the_sub_field('description'); ?>
            </p>
          </div>
        </div>
      <?php endwhile; ?>
    </div>
  </div>
</section>
