"use strict";

gsap.registerPlugin(ScrollTrigger);

var select = function select(e) {
  return document.querySelector(e);
};

var selectAll = function selectAll(e) {
  return document.querySelectorAll(e);
};

var selectId = function selectId(e) {
  return document.getElementById(e);
}; //Revaling Footer


var main = document.querySelector("main");
var footer = document.querySelector("footer");
main.style.marginBottom = footer.offsetHeight + "px";
window.addEventListener("resize", function () {
  main.style.marginBottom = footer.offsetHeight + "px";
}); // Navbar color change

$(window).scroll(function () {
  var scroll = $(window).scrollTop();

  if (scroll >= 20) {
    $("header").addClass("HeaderBg");
  } else {
    $("header").removeClass("HeaderBg");
  }
}); // Mobile Menu

$(document).ready(function () {
  $(".menuToggler a").click(function (e) {
    e.preventDefault();
    $(".overlay").fadeToggle(200);
    $("body").addClass("overflowhidden");
    $(this).toggleClass("btn-open").toggleClass("btn-close");
  });
});
$(".MenuCloser").on("click", function () {
  $(".overlay").fadeToggle(200);
  $("body").removeClass("overflowhidden");
  $(".menuToggler a").toggleClass("btn-open").toggleClass("btn-close");
  open = false;
}); // Inital Animations

var checkLoadedFunction = function checkLoadedFunction() {
  startAnimation();
};

var startAnimation = function startAnimation() {
  gsap.utils.toArray(".gs_reveal_heading").forEach(function (elem) {
    var newText = elem.textContent.trim().split(" ").map(function (i) {
      return "<span> ".concat(i, " </span>");
    });
    elem.innerHTML = newText.join(" ");
    var textToAnimate = Array.from(elem.querySelectorAll("span"));
    gsap.set(textToAnimate, {
      yPercent: 150,
      autoAlpha: 0
    });
    ScrollTrigger.create({
      trigger: textToAnimate,
      once: true,
      onEnter: function onEnter() {
        return gsap.to(textToAnimate, {
          // y: 0,
          // opacity: 1,
          //duration: 1,
          stagger: 0.075,
          autoAlpha: 1,
          // delay: 0.4,
          // ease: 'power3.out',
          overwrite: "auto",
          yPercent: 0,
          duration: 1.25,
          ease: "power3.out"
        });
      },
      onEnterBack: function onEnterBack() {},
      onLeave: function onLeave() {}
    });
  });
  gsap.utils.toArray(".gs_reveal").forEach(function (elem) {
    gsap.set(elem, {
      y: 50,
      opacity: 0,
      autoAlpha: 0
    });
    ScrollTrigger.create({
      trigger: elem,
      once: true,
      onEnter: function onEnter() {
        return gsap.to(elem, {
          y: 0,
          opacity: 1,
          duration: 1,
          stagger: 0.2,
          autoAlpha: 1,
          delay: 0.3,
          ease: "power3.out",
          overwrite: "auto"
        });
      },
      onEnterBack: function onEnterBack() {},
      onLeave: function onLeave() {}
    });
  });
  gsap.utils.toArray(".gs_fade_reveal").forEach(function (elem) {
    gsap.set(elem, {
      opacity: 0,
      y: 10,
      autoAlpha: 0
    });
    ScrollTrigger.create({
      trigger: elem,
      once: true,
      onEnter: function onEnter() {
        return gsap.to(elem, {
          opacity: 1,
          duration: 1,
          stagger: 0.2,
          y: 0,
          autoAlpha: 1,
          delay: 0.35,
          ease: "power3.out",
          overwrite: "auto"
        });
      },
      onEnterBack: function onEnterBack() {},
      onLeave: function onLeave() {}
    });
  });
}; // Five cards text animations


function FiveCards() {
  var tl = gsap.timeline({
    scrollTrigger: {
      trigger: ".Cards",
      once: true,
      //toggleActions: 'restart none none none',
      start: "center bottom"
    }
  });
  tl.from(".Rollup", {
    ease: "expo",
    y: "+=6vh",
    duration: 1.3,
    opacity: 0,
    stagger: 0.25
  });
}

if (document.querySelector(".Rollup")) {
  FiveCards();
} // Text gradient rotation animation


var rotateGradientPoint = function rotateGradientPoint() {
  var allPoint = Array.from(document.querySelectorAll(".gradBul-js"));
  var counter = 0;
  setInterval(function () {
    allPoint.forEach(function (item) {
      item.classList.remove("activeBullet");
    });
    allPoint[counter].classList.add("activeBullet");
    counter = counter + 1;

    if (counter > allPoint.length - 1) {
      counter = 0;
    }
  }, 4500);
};

if (document.querySelector(".gradBul-js")) {
  rotateGradientPoint();
} // Logo Slider - Slick Slider


function Logoslider() {
  $(".fourlogoslider").slick({
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    responsive: [{
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 400,
      settings: {
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
}

Logoslider(); // Testimonial Slider - Slick Slider

var testinomialSlider = function testinomialSlider() {
  var getAnimationSettings = function getAnimationSettings() {
    var easing = Power1.Elastic;
    return {
      easing: easing
    };
  };

  var getAnimationItems = function getAnimationItems() {
    // Find all items
    var slideTitle = document.querySelectorAll("slideTitle");
    var slideDescription = document.querySelectorAll(".slideDescription");
    var slideSubTitle = document.querySelectorAll(".slideSubtitle");
    var slideCtaWrapper = document.querySelectorAll(".slideCtaWrapper"); // // Merge all elements into one array to set invisible

    var allSlidesAllElements = [slideTitle, slideDescription, slideSubTitle, slideCtaWrapper]; // Find active slide

    var activeSlide = $(".slick-slide.slick-current.slick-active"); // Find items in current slide

    var activeSlideTitle = activeSlide.find(".slideTitle");
    var activeSlideDescription = activeSlide.find(".slideDescription");
    var activeSlideSubTitle = activeSlide.find(".slideSubtitle");
    var activeSlideCtaWrapper = activeSlide.find(".slideCtaWrapper");
    var allActiveSlideAllElements = [activeSlideTitle, activeSlideDescription, activeSlideSubTitle, activeSlideCtaWrapper]; // Return allelements and active elements

    return {
      allActiveSlideAllElements: allActiveSlideAllElements,
      allSlidesAllElements: allSlidesAllElements,
      activeSlide: activeSlide,
      activeSlideTitle: activeSlideTitle,
      activeSlideSubTitle: activeSlideSubTitle,
      activeSlideDescription: activeSlideDescription,
      activeSlideCtaWrapper: activeSlideCtaWrapper
    };
  };

  var setAnimationTimelines = function setAnimationTimelines() {
    var animationItems = getAnimationItems();
    var titleTimeline = gsap.timeline({
      paused: true
    });
    titleTimeline.from(animationItems.activeSlideTitle, {
      opacity: 0,
      y: 200
    }).to(animationItems.activeSlideTitle, {
      opacity: 1,
      y: 0
    });
    var slideDescriptionTimeline = gsap.timeline({
      paused: true
    });
    slideDescriptionTimeline.from(animationItems.activeSlideDescription, {
      opacity: 0,
      y: 200
    }).to(animationItems.activeSlideDescription, {
      opacity: 1,
      y: 0
    });
    var slideSubTitleTimeline = gsap.timeline({
      paused: true
    });
    slideSubTitleTimeline.from(animationItems.activeSlideSubTitle, {
      opacity: 0,
      y: 200
    }).to(animationItems.activeSlideSubTitle, {
      opacity: 1,
      y: 0
    });
    var slideCtaWrapperTimeline = gsap.timeline({
      paused: true
    });
    slideCtaWrapperTimeline.from(animationItems.activeSlideCtaWrapper, {
      opacity: 0,
      y: 200
    }).to(animationItems.activeSlideCtaWrapper, {
      opacity: 1,
      y: 0
    });
    return {
      titleTimeline: titleTimeline,
      slideDescriptionTimeline: slideDescriptionTimeline,
      slideSubTitleTimeline: slideSubTitleTimeline,
      slideCtaWrapperTimeline: slideCtaWrapperTimeline
    };
  };

  var setMasterTimeline = function setMasterTimeline() {
    var animationSettings = getAnimationSettings();
    var animationTimelines = setAnimationTimelines();
    var sliderMasterTimeline = new TimelineMax({
      ease: animationSettings.ease,
      paused: true
    });
    sliderMasterTimeline // .add(setSlideTimeline(), 0) //Tried this but no dice
    .add(animationTimelines.titleTimeline.play(), 0.2).add(animationTimelines.slideSubTitleTimeline.play(), 1.25).add(animationTimelines.slideDescriptionTimeline.play(), 0.75).add(animationTimelines.slideCtaWrapperTimeline.play(), 1.5); // manipulates global animation speed

    sliderMasterTimeline.timeScale(1);
    return sliderMasterTimeline;
  }; // $('.testimonialslider').on('init', function () {
  //   const masterTimeline = setMasterTimeline();
  //   masterTimeline.play();
  // });


  var slider = $(".testimonialslider").slick({
    dots: false,
    pauseOnFocus: false,
    arrows: true,
    drag: true,
    infinite: true,
    speed: 500,
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1
  });
};

testinomialSlider(); // Video play on click functionality

var videoBtn = document.querySelector(".js-videoPoster");

var playVideo = function playVideo() {
  var video = document.querySelector(".js-videoIframe");
  videoBtn.addEventListener("click", function () {
    if (video.paused) {
      video.play();
      videoBtn.style.opacity = 0;
      video.setAttribute("controls", "true");
    } else {
      video.pause();
      videoBtn.style.opacity = 1;
    }
  });
  var isPaused = false;
  var observer = new IntersectionObserver(function (entries, observer) {
    entries.forEach(function (entry) {
      if (entry.intersectionRatio != 1 && !video.paused) {
        video.pause();
        isPaused = true;
      } else if (isPaused) {
        video.play();
        isPaused = false;
      }
    });
  }, {
    threshold: 0.5
  });
  observer.observe(video);
};

if (videoBtn) {
  playVideo();
} // Gallery Functionality


function openTab(evt, name) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tab-content");

  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  tablinks = document.getElementsByClassName("tab-links");

  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  document.getElementById(name).style.display = "block";
  evt.currentTarget.className += " active";
}

function galleryblock() {
  var gallery = document.querySelector("#gallery");

  var getVal = function getVal(elem, style) {
    return parseInt(window.getComputedStyle(elem).getPropertyValue(style));
  };

  var getHeight = function getHeight(item) {
    return item.querySelector(".content").getBoundingClientRect().height;
  };

  var resizeAll = function resizeAll() {
    var altura = getVal(gallery, "grid-auto-rows");
    var gap = getVal(gallery, "grid-row-gap");
    gallery.querySelectorAll(".gallery-item").forEach(function (item) {
      var el = item;
      el.style.gridRowEnd = "span " + Math.ceil((getHeight(item) + gap) / (altura + gap));
    });
  };

  gallery.querySelectorAll("img").forEach(function (item) {
    item.classList.add("byebye");

    if (item.complete) {
      console.log(item.src);
    } else {
      item.addEventListener("load", function () {
        var altura = getVal(gallery, "grid-auto-rows");
        var gap = getVal(gallery, "grid-row-gap");
        var gitem = item.parentElement.parentElement;
        gitem.style.gridRowEnd = "span " + Math.ceil((getHeight(gitem) + gap) / (altura + gap));
        item.classList.remove("byebye");
      });
    }
  });
  window.addEventListener("resize", resizeAll);
  gallery.querySelectorAll(".gallery-item").forEach(function (item) {
    item.addEventListener("click", function () {
      item.classList.toggle("full");
    });
  });
}

$(window).load(function () {// $('#preloader').delay(50).fadeOut(100); // will fade out the white DIV that covers the website.
  // $('body').delay(50).css({'overflow':'visible'});
  //   initGsap();
  //   Logoslider();
  //   accordians();
  //   galleryblock();
  //   // preloader();
}); // Gallery Magnification Functionality

$(".image-popup-vertical-fit").magnificPopup({
  type: "image",
  mainClass: "mfp-with-zoom",
  gallery: {
    enabled: true
  },
  zoom: {
    enabled: true,
    duration: 300,
    // duration of the effect, in milliseconds
    easing: "ease-in-out",
    // CSS transition easing function
    opener: function opener(openerElement) {
      return openerElement.is("img") ? openerElement : openerElement.find("img");
    }
  }
});
$(".openVideo").magnificPopup({
  type: "inline",
  callbacks: {
    open: function open() {
      $("html").css("margin-right", 0); // Play video on open:

      $(this.content).find("video")[0].play();
    },
    close: function close() {
      // Reset video on close:
      $(this.content).find("video")[0].load();
    }
  }
}); // Faq accordion

var openCloseAccordion = function openCloseAccordion() {
  var allAccordionItem = Array.from(document.querySelectorAll(".accordionItem "));
  allAccordionItem.forEach(function (item) {
    item.addEventListener("click", function () {
      item.classList.toggle("open");
    });
  });
};

if (document.querySelector(".accordionItem")) {
  openCloseAccordion();
} // Contact Form Submission


$(document).ready(function () {
  if (select(".ContactForm")) {
    //  const phoneRegex = /^[\d,\s,\+,\-]{5,20}/;
    var nameRegex = /^([a-zA-Z ]){2,30}$/;
    var msgRegex = /^[^<\\/{}*%=>:|;#]*$/;
    var phoneInput = document.querySelector("#intl-phone");
    var iti = intlTelInput(phoneInput);
    intlTelInput(phoneInput, {
      initialCountry: "AE",
      // any initialisation options go here
      utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.12/js/utils.min.js"
    });
    var errormessageDiv = document.querySelector(".error-message");

    var showErrorMessage = function showErrorMessage(text, msgDiv) {
      msgDiv.style.display = "block";
      msgDiv.style.padding = "0.8rem";
      msgDiv.style.color = "#D8000C";
      msgDiv.style.background = "#FFD2D2";
      msgDiv.innerText = text;
    };

    var hideErrorMessage = function hideErrorMessage(msgDiv) {
      msgDiv.style.display = "none";
    };

    phoneInput.addEventListener("blur", function () {
      var errorBlock = select(".phone-error");

      if (iti.isValidNumber()) {
        // console.log("valid number");
        hideErrorMessage(errorBlock);
      } else {
        // console.log("invalid number");
        showErrorMessage("Invalid Phone Number", errorBlock);
      }
    });
    select("#name").addEventListener("blur", function () {
      var errorBlock = select(".name-error"); // console.log($("#name").val());

      if (nameRegex.test($("#name").val())) {
        hideErrorMessage(errorBlock);
      } else {
        // console.log("invalid name");
        showErrorMessage("Name field can contain A-Z, a-z and space", errorBlock);
      }
    });
    select("#msg").addEventListener("blur", function () {
      var errorBlock = select(".msg-error");

      if (msgRegex.test($("#msg").val())) {
        hideErrorMessage(errorBlock);
      } else {
        showErrorMessage("Message field cannot have following characters [^<\\/{}*%=>:|;#]", errorBlock);
      }
    });
    $("#contact_form").submit(function (event) {
      // console.log("clicked");
      var submitBtn = document.querySelector(".submitBtn");
      event.preventDefault();
      submitBtn.value = "Sending..."; // console.log(iti.getNumber(intlTelInputUtils.numberFormat.E164));

      $.ajax({
        url: "mail.php",
        type: "POST",
        data: JSON.stringify({
          name: $("#name").val(),
          email: $("#email").val(),
          contactNumber: iti.getNumber(intlTelInputUtils.numberFormat.E164),
          message: $("#msg").val()
        }),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function success(response) {
          if (response) {
            if (response["error"]) {
              var message = document.querySelector(".error-message"); //console.log(response);

              submitBtn.value = "Oops";
              var databaseError = response.errorDetails.includes("mysqli_");
              var formatError = response.errorDetails.includes("fails");
              var toManyReq = response.errorDetails.includes("too many request");
              message.style.display = "block";
              message.style.padding = "0.8rem";
              message.style.color = "#D8000C";
              message.style.background = "#FFD2D2";
              submitBtn.value = "Oops";

              if (databaseError) {
                message.innerText = "Database Error. Please try again after sometimes";
              } else if (toManyReq) {
                message.innerText = "Too many requests. Please try again after sometimes";
              } else if (formatError) {
                message.innerText = "Please fill in required format";
              } else message.innerText = "Something went wrong. Please try again after sometimes";

              setTimeout(function () {
                message.style.display = "none";
                message.innerText = "";
                $("form")[0].reset();
                submitBtn.value = "Send";
              }, 5000);
            } else {
              var _message = document.querySelector(".error-message"); //console.log(response, "response");


              _message.style.display = "block";
              _message.style.color = "#4F8A10";
              _message.style.background = "#DFF2BF";
              _message.style.padding = "0.8rem";
              _message.innerText = "We have successfully received you email. We will get back to you soon";
              submitBtn.value = "Success";
              $("form")[0].reset();
              setTimeout(function () {
                _message.style.display = "none";
                _message.innerText = "";
                submitBtn.value = "Send";
              }, 5000); //window.location = "thank-you.php";
            }
          }
        },
        error: function error(err) {
          // console.log(err, "err");
          submitBtn.value = "Oops";
          var message = document.querySelector(".error-message");
          var databaseError = err.responseText.includes("mysqli_");
          var toManyReq = err.responseText.includes("too many request");
          message.style.display = "block";

          if (databaseError) {
            message.innerText = "Database Error. Please try again after sometimes";
          } else if (toManyReq) {
            message.innerText = "Too many requests. Please try again after sometimes";
          } else message.innerText = "Something went wrong. Please try again after sometimes"; // $('form')[0].reset();


          setTimeout(function () {
            message.style.display = "none";
            message.innerText = "";
          }, 5000);
        }
      });
    });
  }
}); // Video play on hover functionality

var playOnHover = function playOnHover() {
  var videoBlock = document.querySelectorAll(".OnhoverVideoBlock > video");
  videoBlock.forEach(function (item) {
    setTimeout(function () {
      //item.setAttribute("autoplay", "");
      item.play();
    }, 3000);
  });
};

window.addEventListener("load", function () {
  if (document.querySelector(".OnhoverVideoBlock")) {
    playOnHover();
  }

  console.clear();
  console.log("Delivered by Cobold Digital. To see more of our work, see cobold.in");
}); //
// Commented Codes
// function initBodyColor() {
//     console.log("White body:")
//     gsap.utils.toArray(".commonSection").forEach(function(elem) {
//     var whiteBody = elem.getAttribute('data-section');
//     	console.log("White body....", whiteBody)
//         // ScrollTrigger.create({
//         //     trigger: elem,
//         //     start:'top 50%',
//         //     end:'bottom 50%',
//         //     onEnter: () => $('body').addClass(whiteBody)
//         //     onLeave: () => $('body').removeClass(whiteBody)
//         //     onLeaveBack: () => $('body').removeClass(whiteBody)
//         //     onEnterBack: () => $('body').addClass(whiteBody)
//         //     // onEnter: () => gsap.to('body', {backgroundColor:color,}),
//         //     // onLeave: () => gsap.to('body', {backgroundColor:'white'}),
//         //     // onLeaveBack: () => gsap.to('body', {backgroundColor:'white'}),
//         //     // onEnterBack: () => gsap.to('body', {backgroundColor:color}),
//         //     // markers:true
//         // });
//     });
// }
// function rightContentAnimate(){
// 	var tl = gsap.timeline({
//         scrollTrigger:{
//             trigger: '.Content',
//             toggleActions: "restart none none none",
//             // start: "top center",
//             // markers: true,
//         }
//     });
// 	tl.from('.list', {
//         ease: "power4",
//         y: "+=4vh",
//         duration: 0.5,
//         scrub: true,
//         opacity: 0,
//     })
//     .from('.listpoints', {
//         y: 100,
//         duration: 0.6,
//         ease: "power4",
//         stagger: 0.3,
//     },0.65)
// }
// function animateFrom(elem, direction) {
//   direction = direction | 1;
//   var x = 0,
//     y = direction * 150;
//   if (elem.classList.contains('gs_reveal_fromLeft')) {
//     x = -100;
//     y = 0;
//   } else if (elem.classList.contains('gs_reveal_fromRight')) {
//     x = 100;
//     y = 0;
//   }
//   gsap.fromTo(
//     elem,
//     { x: x, y: y, autoAlpha: 0 },
//     {
//       duration: 0.9,
//       x: 0,
//       y: 0,
//       autoAlpha: 1,
//       ease: 'power2.in',
//       overwrite: 'auto',
//       stagger: {
//         amount: 1.5,
//       },
//     }
//   );
// }
// function hide(elem) {
//   gsap.set(elem, { autoAlpha: 0 });
// }
// window.addEventListener('load', (event) => {
//   const preload = document.querySelector('#preloader');
//   setTimeout(() => {
//     preload.style.transform = 'translateY(-100vh)';
//   }, 100);
// });
// $(document).on('click', '.js-videoPoster', function (e) {
//   e.preventDefault();
//   var poster = $(this);
//   var wrapper = poster.closest('.js-videoWrapper');
//   videoPlay(wrapper);
// });
// function videoPlay(wrapper) {
//   var iframe = wrapper.find('.js-videoIframe');
//   if (iframe[0].pause) {
//     iframe[0].play();
//     wrapper.addClass('videoWrapperActive');
//   } else {
//     iframe[0].pause();
//     wrapper.removeClass('videoWrapperActive');
//   }
//   //var src = iframe.data('src');
//   //iframe.attr('src', src);
// }
// function accordians(){
// 	var accItem = document.getElementsByClassName('accordionItem');
// 	var accHD = document.getElementsByClassName('accordionItemHeading');
// 	for (i = 0; i < accHD.length; i++) {
// 		var i;
// 	    accHD[i].addEventListener('click', toggleItem, false);
// 	}
// 	function toggleItem() {
// 	    var itemClass = this.parentNode.className;
// 	    for (i = 0; i < accItem.length; i++) {
// 	    	var i;
// 	        accItem[i].className = 'accordionItem close';
// 	    }
// 	    if (itemClass == 'accordionItem close') {
// 	        this.parentNode.className = 'accordionItem open';
// 	    }
// 	}
// }
// function initGsap() {
//     initBodyColor();
//     // contentAnimate();
//     // rightContentAnimate();
//     videoCardsBlock();
//     TeamCardsBlock();
//     CenterThreeCards();
//     FiveCards();
//     // GalleryCards();
//     // TopBottomAnimation();
//     playbutton();
//     LogoTestimonial();
// }
// function GalleryCards(){
//     var tl = gsap.timeline({
//         scrollTrigger: {
//             trigger: '.Cards',
//             toggleActions: "restart none none none",
//             start: "center top",
//         }
//     });
//     tl
//     .from(".item", {
//         ease: "expo",
//         y: "+=2vh",
//         duration: 1,
//         opacity: 0,
//         stagger: 0.15,
//     })
// }
// function playbutton(){
// 	gsap.to(".play", {
// 	  scrollTrigger: {
// 	    trigger: ".VideoContainer",
//       toggleActions: 'play none none reset',
// 	    scrub: 1,
// 	    start: "top bottom",
// 	    end: "bottom -300%",
//         // once: true,
// 	  },
// 	  y: "-100%"
// 	});
// }
// function LogoTestimonial(){
//     var tl = gsap.timeline({
//         scrollTrigger: {
//             trigger: '.Animateblock',
//             toggleActions: "restart none none none",
//             start: "center bottom",
//         }
//     });
//     tl
//     .from(".logo", {
//         ease: "expo",
//         y: "+=6vh",
//         duration: 2,
//         opacity: 0,
//         stagger: 0.25,
//     })
// }
// function videoPlay(wrapper) {
// 	var iframe = wrapper.find('.js-videoIframe');
// 	var src = iframe.data('src');
// 	wrapper.addClass('videoWrapperActive');
// 	iframe.attr('src', src);
//}
// var body = document.querySelector("body");
// const darkBg = gsap.utils.toArray(".darkBg");
// darkBg.forEach((item) => {
//     gsap.to(item, {
//       scrollTrigger: {
//         trigger: item,
//         start: "top +=90",
//         // end: "+=100",
//         scrub: true,
//         // markers: true,
//         toggleClass:  "title-selected",
//         onEnter: () => body.classList.add("bodyDark"),
//         onLeave: () => body.classList.remove("bodyDark"),
//         onEnterBack: () => body.classList.add("bodyDark"),
//         onLeaveBack: () => body.classList.remove("bodyDark"),
//       }
//     })
// });
// var body = document.querySelector("body");
// const videoBg = gsap.utils.toArray(".videoBg");
// videoBg.forEach((item) => {
//     gsap.to(item, {
//       scrollTrigger: {
//         trigger: item,
//         start: "top +=90",
//         // end: "+=100",
//         scrub: true,
//         // markers: true,
//         toggleClass:  "title-selected",
//         onEnter: () => body.classList.add("bodyDark"),
//         onLeave: () => body.classList.remove("bodyDark"),
//         onEnterBack: () => body.classList.add("bodyDark"),
//         onLeaveBack: () => body.classList.remove("bodyDark"),
//       }
//     })
// });
// var body = document.querySelector("body");
// const testimonial = gsap.utils.toArray(".testimonial");
// testimonial.forEach((item) => {
//     gsap.to(item, {
//       scrollTrigger: {
//         trigger: item,
//         start: "top +=90",
//         // end: "+=100",
//         scrub: true,
//         // markers: true,
//         toggleClass:  "title-selected",
//         onEnter: () => body.classList.add("testimonialDark"),
//         onLeave: () => body.classList.remove("testimonialDark"),
//         onEnterBack: () => body.classList.add("testimonialDark"),
//         onLeaveBack: () => body.classList.remove("testimonialDark"),
//       }
//     })
// });
// var body = document.querySelector("body");
// const LeftDarkRightContent = gsap.utils.toArray(".LeftDarkRightContent");
// LeftDarkRightContent.forEach((item) => {
//     gsap.to(item, {
//       scrollTrigger: {
//         trigger: item,
//         start: "top +=90",
//         // end: "+=100",
//         scrub: true,
//         // markers: true,
//         toggleClass:  "title-selected",
//         onEnter: () => body.classList.add("LeftDarkRightWhite"),
//         onLeave: () => body.classList.remove("LeftDarkRightWhite"),
//         onEnterBack: () => body.classList.add("LeftDarkRightWhite"),
//         onLeaveBack: () => body.classList.remove("LeftDarkRightWhite"),
//       }
//     })
// });
// var body = document.querySelector("body");
// const whiteBg = gsap.utils.toArray(".whiteBg");
// whiteBg.forEach((item) => {
//     gsap.to(item, {
//       scrollTrigger: {
//         trigger: item,
//         start: "top +=60",
//         // end: "+=100",
//         scrub: true,
//         // markers: true,
//         toggleClass:  "title-selected",
//         onEnter: () => body.classList.add("bodyWhite"),
//         onLeave: () => body.classList.remove("bodyWhite"),
//         onEnterBack: () => body.classList.add("bodyWhite"),
//         onLeaveBack: () => body.classList.remove("bodyWhite"),
//       }
//     })
// });
// function videoCardsBlock(){
//     var tl = gsap.timeline({
//         scrollTrigger: {
//             trigger: '.ThreeCardsBlock',
//             toggleActions: "restart none none none",
//             start: "center bottom",
//         }
//     });
//     tl
//     .from(".videocontent", {
//         ease: "expo",
//         y: "+=6vh",
//         duration: 2,
//         opacity: 0,
//         stagger: 0.15,
//     })
// }
// function TeamCardsBlock(){
//     var tl = gsap.timeline({
//         scrollTrigger: {
//             trigger: '.ThreeCardsBlock',
//             toggleActions: "restart none none none",
//             start: "center bottom",
//         }
//     });
//     tl
//     .from(".teamContent", {
//         ease: "stepped",
//         y: "+=6vh",
//         duration: 2,
//         opacity: 0,
//         stagger: 0.35,
//     })
// }
// function CenterThreeCards(){
//     var tl = gsap.timeline({
//         scrollTrigger: {
//             trigger: '.HeadingWithBg',
//             toggleActions: "restart none none none",
//             start: "top bottom",
//         }
//     });
//     tl
//     .from(".teamContent", {
//         ease: "expo",
//         y: "+=6vh",
//         duration: 2,
//         opacity: 0,
//         // stagger: 0.25,
//     })
// }