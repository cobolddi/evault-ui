<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package evault
 */

get_header();
?>
		<section class="Section">
		  <div class="container">
		    <div class="general_content">
		      <h2><?php the_title(); ?></h2>
		      <?php the_content(); ?>
		    </div>
		  </div>
		</section>
		<script>
		  var x = document.getElementById("header");
		  x.classList.add("NormalHeader");
		</script>
<?php
get_footer();
