<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'evault' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '123456' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
define( 'FS_METHOD', 'direct' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'l4H<$21Yqfo@Bg`W9Vo6e asd5]^xBK(?4J]l!o[3AA:a#@mUp;1bI}h)60._J@n' );
define( 'SECURE_AUTH_KEY',  'U|!mgD;0? WWhCK>Qhqvj]A?P_?dzD;I{*Jc6jk~ _n>Y:`P[ 2b1EMth=D~kjxX' );
define( 'LOGGED_IN_KEY',    'H9LKy@g7x)3nweNj%A1*:.0]J=]-$j>27<qn@Wi%0u6bE;cmc%}qFV-<n~}X6*.}' );
define( 'NONCE_KEY',        'k>XyuKC+453/o=DV^^-~Q/xw<.ji-/Ox+gU$}>7>hBh_A9a+>v*{)Kk>;u-MI?nG' );
define( 'AUTH_SALT',        '`S3^>jF?nuzSY*wpbkAL9E[3DwUt4Jb{6R_7e7B4`*MS]yy>z@^&$p~<Em3HExjH' );
define( 'SECURE_AUTH_SALT', 'C)r)p1-6iz_Lm|n@%tKKU_38[AzGBB@J.x7br+EQ<^<e,m*(G{r#QYZ?Lq]Lo*wa' );
define( 'LOGGED_IN_SALT',   'U:oaTsb%41;6N(nCNpJp)sW;Qo@phk>/tgM571=;Kn|Y]{%I?W>$3f6f]2W+e(GQ' );
define( 'NONCE_SALT',       'Pf/y}R9^RX5]w_>4nUc~BG66z1T9N0bkBN)dx=p/:*rZrSg{}j!;$(<T@Ke8H 85' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'evault_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
