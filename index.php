<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/PageHeader/Homebanner.php') ?>
<?php @include('template-parts/RightContentWithBg.php') ?>

<section class="HomeBanner Reverse">

		<div class="Video">
			<img src="assets/img/evault/Home/2.jpg" />
		</div>
		<div class="BannerContent">
			<div class="BannerText">
				<div class="homeBanner">
					<div class="delay_15">
                    <h2 class="gs_reveal_heading large-headings">
						<!-- No risk, all&nbsp;reward -->
					عالم من المكافآت – بلا مخاطرات 
					</h2>
						<h4 class="GradientText gs_fade_reveal">
							<!-- A unique promotion coverage solution. -->
						حلول تسويقية فريدة للحملات الترويجية. 
						</h4>
						<p class="gs_fade_reveal">
							
		<!-- The E-Vault insures you against the risk of prize redemption, helping you achieve your marketing goals while keeping your financial risk low. -->
	تضمن لكم إيڤولت التأمين ضد مخاطر تحصيل الجوائز، ما يساعدك على تحقيق أهدافك التسويقية مع تقليل المخاطر المالية المحتملة. 
	</p>
					</div>	
				</div>
			</div>
		</div>

</section>


<section class="HomeBanner Equal">
		<div class="Video">
			<img src="assets/img/evault/Home/3.jpg" />
		</div>
		<div class="BannerContent">
			<div class="BannerText">
				<div class="homeBanner">
					<div class="delay_15">
                    <div class="Content">
						<h2 class="large-headings mb1-6">
					<span class="gs_reveal_heading" style="display: block;">
					<!-- Free to play -->
				إلعبوا مجاناً
				</span>
					<span class="gs_reveal_heading" style="display: block;">
					<!-- Free to participate -->
				شاركوا مجاناً

				</span></h2>
						<h4 class=" GradientText gs_fade_reveal">
							<!-- Everyone is invited to play. -->
						لمشاركة متاحة للجميع.
						</h4>
						<p class="gs_fade_reveal">
							
						<!-- Maximize the reach of your campaign by inviting everyone to try their luck at winning big. -->
					وسع نطاق تأثير حملتك التسويقية إلى أقصى حد بدعوة الجميع لتجربة حظهم للفوز بجائزة كبرى.
					</p>
						<ul class="lists">
							<li class="bulletPoints gradBul-js"><span>
								<!-- No eligibility clause or mandatory purchase required -->
							لا يتطلب وجود شرط الأهلية أو الشراء الإلزامي


							</span></li>
							<li class="bulletPoints gradBul-js"><span>
								
							<!-- Attract large crowds and ensure maximum participation -->
						اجتذب حشوداً ضخمة وتأكد من تحقيق أكبر قدر من المشاركة 
						</span></li>
							<li class=" bulletPoints gradBul-js"><span>
								
							<!-- Ensure wider reach and increased brand exposure -->
							تأكد من توسيع نطاق الانتشار وزيادة التعريف بالعلامة التجارية  
						</span></li>
						</ul>
					</div>	
				</div>
			</div>
		</div>
</section>


<?php // @include('template-parts/RightImgLeftContent.php') ?>

<?php @include('template-parts/ThreeCardsBlock.php') ?>

<?php @include('template-parts/BgVideoWithFiveCards.php') ?>

<?php @include('template-parts/VideoPlaySection.php') ?>

<?php @include('template-parts/LogotestimonialBlock.php') ?>

<?php @include('template-parts/footer.php') ?>
