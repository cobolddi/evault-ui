<?php @include('template-parts/header.php') ?>

    <main>
      <section class="HomeBanner whoWeAreHeader">
        <div class="BannerContent">
          <div class="BannerText">
            <div class="homeBanner">
              <div class="delay_15">
                <h2 class="gs_reveal_heading large-headings mb1-6">
                  About E-Vault
                </h2>
                <h4 class="gs_reveal GradientText">Your success, our story.</h4>
                <p class="gs_reveal">
                  5th Consulting DMCC has been in the region for five years and
                  is part of the EMIRAT group of companies, headquartered in
                  Munich, Germany.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="Video">
          <img
            src="assets/img/evault/who we are/img-1.jpg"
            alt=""
          />
        </div>
      </section>

      <?php @include('template-parts/CenterTopIconBtmContent.php') ?>
      <?php @include('template-parts/team.php') ?>


    </main>

      <?php @include('template-parts/footer.php') ?>