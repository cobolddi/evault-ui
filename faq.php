<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Evault</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="Automation of task with minification of css and js">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png">

	<link href="assets/css/vendor.min.css" rel="stylesheet">
	<link href="assets/css/styles.min.css" rel="stylesheet">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/PreloadJS/1.0.1/preloadjs.min.js"></script>
</head>

<body class="NoOverflow rtl" id="body">

	<header id="header" class="headerspace NormalHeader">
		<div class="container">
			<div class="row">
				<div class="col-6 col-md-5">
					<div class="LogoBox">
						<a href="index.php" class="NormalLogo"><img src="assets/img/logo.svg" alt="Evault"></a>
						<!-- <a href="index.php" class="WhiteLogo"><img src="assets/img/white-logo.svg" alt="Evault"></a> -->
					</div>
				</div>
				<div class="col-6 col-md-7 TillIpad">
					<!-- <span class="menu" style="float: right"><a href="">Menu</a></span> -->
					<div class="menu"
						>
						<a class="btn-open" href="#">Menu</a>
					</div>
				</div>
				<div class="col-8 col-md-7 IpadProOnwards">
					<div class="NavigationBlock">
						<nav>
							<ul>
								<!-- <li><a href="index.php">Home</a></li> -->
								<li><a href="how-it-work.php">How it works</a></li>
								<li><a href="gallery.php">Gallery</a></li>
								<li><a href="faq.php">FAQ</a></li>
								<li><a href="who-we-are.php">Who we are</a></li>
								<li><a href="contact-us.php">Contact</a></li>
								<!-- <li class="menu "><a href="#">Menu</a></li> -->
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div class="overlay" style="background: url(assets/img/menu-bg.png) no-repeat;">
		<div class="sub-menu">
			<div class="LogoNav">
				<a href="index.php" class="WhiteLogo"><img src="assets/img/only-white.svg" alt="Evault"></a>
				<span class="Menu"><a>Close</a></span>
			</div>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="how-it-work.php">How it works</a></li>
				<li><a href="gallery.php">Gallery</a></li>
				<li><a href="faq.php">FAQ</a></li>
				<li><a href="who-we-are.php">Who we are</a></li>
				<li><a href="contact-us.php">Contact</a></li>
			</ul>
		</div>
	</div>

	<main>


		<section class="FaqSection">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-4 faqSectionHeading">
						<h2 class="gs_reveal_heading large-headings mb1-6">FAQ's</h2>
						<h4 class="GradientText">You ask, we answer.</h4>
					</div>
					<div class="col-12 col-md-8">
						<div class="accordionWrapper Cards">
							<div class="accordionItem open Rollup">
								<h5 class="accordionItemHeading">
									<span class="question">Can we customize the screen to display our brand logo?</span>
								<span class="plusIcon"></span>
								
								</h5>
								<div class="accordionItemContent">
									<p>Yes, the screen can be customized to display your logo and brand colors.</p>
								</div>
							</div>

							<div class="accordionItem  Rollup">
								<h5 class="accordionItemHeading">
										<span class="question">Do you provide more than one machine per promotion to be deployed at multiple venues?</span>
								<span class="plusIcon"></span>
								</h5>
								<div class="accordionItemContent">
									<p>Yes, we can provide more than one machine, with an additional rental fee applicable per machine.</p>
								</div>
							</div>

							<div class="accordionItem  Rollup">
								<h5 class="accordionItemHeading">
									<span class="question">How can we address hygiene concerns due to Covid19?</span>
								<span class="plusIcon"></span>
								</h5>
								<div class="accordionItemContent">
									<p>We can create a QR Code and display it on the screen where participants can scan it from and play the game on their mobile phones.</p>
								</div>
							</div>

							<div class="accordionItem  Rollup">
								<h5 class="accordionItemHeading">
								<span class="question">Can we change the number of digits in the code to be
									less than or more than six?</span>
								<span class="plusIcon"></span>
								</h5>
								<div class="accordionItemContent">
									<p>Yes, the number of digits can be changed, with each change having an effect on the premium amount. 
									</p>
								</div>
							</div>

							<div class="accordionItem  Rollup">
								<h5 class="accordionItemHeading">
								<span class="question">How can we be assured that the winning code is really
									active?</span>
								<span class="plusIcon"></span>
								</h5>
								<div class="accordionItemContent">
									<p>A sealed security envelope containing the pre-drawn winning combination code is provided to you before the promotion begins. Once the promotion is over, the envelope can be opened, and the winning code can be entered for verification. </p>
								</div>
							</div>

							<div class="accordionItem  Rollup">
								<h5 class="accordionItemHeading">
									<span class="question">Is the E-Vault rented on a per-day basis?</span>
								<span class="plusIcon"></span>
								</h5>
								<div class="accordionItemContent">
									<p>Yes, the E-Vault can be rented on a per-day basis.</p>
								</div>
							</div>

							<div class="accordionItem  Rollup">
								<h5 class="accordionItemHeading">
									
									<span class="question">Is the rent cost different from the premium fee of the promotion?</span>
								<span class="plusIcon"></span>
								</h5>
								<div class="accordionItemContent">
									<p>Yes. The rent cost is fixed whereas the premium fee for the promotion is variable and is decided based on campaign-specific needs and details.</p>
								</div>
							</div>





						</div>
					</div>
				</div>
			</div>
		</section>


		<?php @include('template-parts/footer.php') ?>