<section class="CenterContentOnly WithThreeCards">
	<div class="container">
		<div class="CenterContent">
			<h2 class="gs_reveal_heading large-headings">Personalise your premium</h2>
			<h4 class="gs_fade_reveal GradientText">Rewarding experiences within your budget.</h4>
			<p class="gs_fade_reveal">The premium that covers your risk against reward redemption is carefully calculated and is based on the intended scale of the campaign, prize value and the game mechanics you choose.</p>
		</div>
		<div class="ThreeCardsWrapper Cards">
			<ul>
				<li>
					<h5 class="Rollup">Prize value</h5>
					<p class="Rollup">It all starts here – your<br> intended prize value. Higher<br> the value, higher the risk and<br> hence, higher the premium.</p>
				</li>
				<li>
					<h5 class="Rollup">Number of attempts</h5>
					<p class="Rollup">This relates to the scale and<br> duration of the campaign,<br> and has a direct effect on the<br> premium amount.</p>
				</li>
				<li>
					<h5 class="Rollup">All in the code</h5>
					<p class="Rollup">A 6-digit and an 8-digit secret<br> code change the probability of<br> prize redemption, thus having an<br> impact on the premium amount.</p>
				</li>
			</ul>
		</div>
	</div>
</section>