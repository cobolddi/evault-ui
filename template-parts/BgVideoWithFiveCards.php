<section class="BgVideoWithFiveCards darkBg">
  <video
    src="assets/img/new-video/video-2.mp4"
    autoplay
    loop
    muted
    playsinline
  ></video>
  <div class="container" style="z-index: 2">
    <div class="TopHeading">
      <h2 class="gs_reveal_heading large-headings">
        <!-- Unlock the rewards -->
        افتح للحصول على المكافآت
      </h2>
    </div>
    <div class="FiveCardsBlock">
      <div class="TwoCardsRow">
        <div class="Cards">
          <h3 class="Rollup">
            <!-- Plan, plug and play -->
        خطط، شغل، العب 
          </h3>
          <p class="Rollup">
            <!-- The E-Vault comes <br /> pre-installed and is easy <br /> to install and
            operate. -->

          تأتيك إيڤولت مبرمجة مسبقاً، كل ما عليك هو تثبيتها وتشغيلها. 
          </p>
        </div>
        <div class="Cards">
          <h3 class="Rollup">

            <!-- More visibility, <br /> more footfall -->
            مزيد من الوضوح لمزيد من الإقبال
          </h3>
          <p class="Rollup">
            <!-- With no mandatory <br /> purchase needed, ensure <br /> wider
            participation and <br /> increased exposure. -->

            ع عدم الحاجة إلى الشراء الإلزامي، نضمن أقصى قدر من المشاركة والانتشار. 
          </p>
        </div>
      </div>
      <div class="TwoCardsRow">
        <div class="Cards"> 
          <h3 class="Rollup">
            <!-- Your campaign, <br /> your look -->
          حملتك التسويقية تعكس مظهرك
          </h3>
          <p class="Rollup">
            <!-- Give your campaign your <br /> own look by personalizing <br /> the
            E-Vault with your <br /> brand colors. -->
            عكس مظهرك على حملتك التسويقية من خلال تجسيد إيڤولت بألوان علامتك التجارية.
          </p>
        </div>
        <div class="Cards">
          <h3 class="Rollup">
            <!-- Know your <br /> customer -->
            اعرف عميلك 
          </h3>
          <p class="Rollup">
            <!-- With a captive audience, <br /> easily engage potential <br /> customers
            and generate <br /> valuable leads. -->
            عندما يكون لديك جمهور متشوق، يمكنك بسهولة إشراك العملاء المحتملين وتحقيق فرص قيمة لاستقطاب عملاء جدد. 
          </p>
        </div>
      </div>
      <div class="TwoCardsRow SingleCard">
        <div class="Cards">
          <h3 class="Rollup">
            <!-- Go mobile  -->
            استخدم الهاتف الجوال 
          </h3>
          <p class="Rollup">
            <!-- Scan the QR code <br /> displayed on the digital <br /> screen and play
            the game <br /> on your mobile. -->
            امسح ضوئياً رمز الاستجابة السريعة الظاهر على الشاشة الرقمية، ثم العب اللعبة على هاتفك الجوال.
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
