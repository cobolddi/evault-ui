<section class="HomeBanner">
  <div class="BannerContent">
    <div class="BannerText">
      <div class="homeBanner">
        <div class="delay_15">
          <h1 class="gs_reveal_heading large-headings hero_title" id="title1">
            <!-- It’s time to think inside the&nbsp;box -->
            حان وقت التفكير داخل الصندوق
          </h1>
          <h4 class="gs_reveal GradientText">
            <!-- Unravel a new world of premium rewards. -->
            اكتشف عالماً جديداً مليئاً بالمكافآت المميزة.
          </h4>
          <p class="gs_reveal">
            <!-- A world with unlimited possibilities. A world that empowers you to
            realize the vision of your brand. -->

            عالم مليء بالإمكانيات غير المحدودة، يعطيك القدرة على تحقيق رؤية علامتك التجارية.
          </p>
        </div>
      </div>
    </div>
    <div class="LearnMore">
      <a href="#second-fold" class="">Learn more</a>
    </div>
  </div>
  <div class="Video">
    <video
      src="assets/img/new-video/video-1.mp4"
      loop
      autoplay
      muted
      playsinline
    ></video>
  </div>
</section>
