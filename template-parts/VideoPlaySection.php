<section class="WorkWellVideo videoBg" style="background: #000" id="video" >
  <div
    class="VideoContainer js-videoWrapper"
    style="background: url(assets/img/tempimg/video-bg.png) no-repeat"
    data-aos="zoom-in"
  >
    <video
      src="assets/img/new-video/video-3.mp4"
      poster="assets/img/evault/Home/video-bg.jpg"
      class="videoIframe js-videoIframe"
    ></video>
    <!-- <iframe class="videoIframe js-videoIframe" frameborder="0" allowtransparency="true" allowfullscreen="" data-src="https://www.youtube.com/embed/Qy0SJWgzAvM?autoplay=1&amp;modestbranding=1&amp;rel=0&amp;hl=entirely&amp;showinfo=0&amp;color=white"></iframe> -->
    <button class="videoPoster js-videoPoster">
      <img src="assets/img/play.svg" alt="" class="play" />
    </button>
  </div>
</section>
