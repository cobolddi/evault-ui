<section class="RightImgLeftContent" style="background: url(assets/img/tempimg/rewards.png) no-repeat;">
	<div class="container">
		<div class="LeftContent">
			<div class="row">
				<div class="col-12 col-md-6 MobileOnly">
					<img src="assets/img/tempimg/rewards.png" alt="" style="width: 100%; display: block;">
				</div>
				<div class="col-12 col-md-6">
					<div class="Content">
						<h2 class="gs_reveal">No risk-all reward</h2>
						<h4 class="gs_reveal GradientText">A unique promotion coverage solution</h4>
						<p class="gs_reveal">The E-Vault insures you against the risk of prize<br> redemption, helping you achieve your marketing<br> goals while keeping your financial risk low.</p>
					</div>
				</div>
				<div class="col-12 col-md-6 DesktopOnly">
					
				</div>
			</div>
		</div>
	</div>
</section>


<section class="RightImgLeftContent RightContentLeftImg LeftDarkRightContent" style="background: url(assets/img/tempimg/free.png) no-repeat;">
	<div class="container">
		<div class="LeftContent">
			<div class="row">
				<div class="col-12 col-md-6 MobileOnly">
					<img src="assets/img/tempimg/free.png" alt="" style="width: 100%; display: block;">
				</div>
				<div class="col-12 col-md-6 DesktopOnly">
					
				</div>
				<div class="col-12 col-md-6">
					<div class="Content">
						<h2 class="gs_reveal">Free to play <br>Free to participate</h2>
						<h4 class="gs_reveal GradientText">Everyone is invited to play</h4>
						<p class="gs_reveal">Maximize the reach of your campaign by inviting<br> everyone to try their luck at winning big.</p>
						<ul class="lists">
							<li class="gs_reveal">No eligibility clause or mandatory purchase required</li>
							<li class="gs_reveal">Attract large crowds and ensure maximum participation</li>
							<li class="gs_reveal">Ensure wider reach and increased brand exposure</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

