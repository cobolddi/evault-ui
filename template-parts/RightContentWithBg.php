<section id="second-fold" class="RightContentWithBg darkBg" id="VideoWithContent"  no-repeat;">
	<div class="RightContentbox">
		<h2 class="gs_reveal_heading large-headings">
			<!-- Introducing E-Vault -->
			التعرف على إيڤولت
		</h2>
		<h4 class=" GradientText gs_fade_reveal">
		<!-- Your key to unlocking rewards. -->
	طريقك لإطلاق المكافآت.  
	</h4>
		<p class="gs_fade_reveal">
			<!-- Explore the hidden potential of your marketing campaign by using premium rewards to create a larger impact for your brand. -->
			اكتشف الإمكانيات الخفية لحملتك التسويقية بجوائز مميزة لجعل علامتك التجارية أكثر تأثيراً.
		</p>
	</div>
</section>

