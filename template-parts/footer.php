
</main>
<footer>
	<div class="container">
		<div class="Logoblock">
			<a href="index.php"><img src="assets/img/footer-logo.svg" alt=""></a>
		</div>
		<div class="AddressWithSocial">
			<div class="AddressBlock">
				<div class="Address">
					<h4><a href="https://www.google.com/maps/place/5th+Consulting+DMCC/@25.0692279,55.1403336,17z/data=!3m1!4b1!4m5!3m4!1s0x3e5f6d65f06c9c03:0x13dc0cfda9fda365!8m2!3d25.0692279!4d55.1425223" target="blank"><img src="assets/img/add-icon.svg" alt=""></a>UAE</h4>
					<p>RM 4204, Platinum Tower, Cluster I, JLT, Dubai</p>
					<p><a href="mailto:info@5th.ae">info@5th.ae</a></p>
				</div>
				<div class="Address">
					<h4><a href="https://www.google.com/maps/place/EMIRAT+AG/@48.1578031,11.5724886,17z/data=!3m1!4b1!4m8!1m2!2m1!1sEMIRAT+AG!3m4!1s0x479e7510943f79c5:0xaa7fe7363dba3626!8m2!3d48.1578031!4d11.5746773" target="blank"><img src="assets/img/add-icon.svg" alt=""></a>GERMANY </h4>
					<p>Elisabethpl. 1, 80796 München</p>
					<p><a href="mailto:info@emirat.de">info@emirat.de</a></p>
				</div>
			</div>
			<div class="SocialBlock">
				<ul>
					<li><a href="https://www.facebook.com/5thConsultingDMCC"><img src="assets/img/fb-icon.svg" alt=""></a></li>
					<li><a href="https://www.instagram.com/5thconsulting.dmcc/"><img src="assets/img/insta-icon.svg" alt=""></a></li>
					<li><a href="https://www.linkedin.com/company/13278807/admin/"><img src="assets/img/linkedin-icon.svg" alt=""></a></li>
				</ul>
			</div>
		</div>
		<div class="CopyrightWithLinks">
			<div class="Copyright">
				<p>© 5th Consulting DMCC. All Rights Reserved.
</p>
			</div>
			<div class="QuickLink">
				<ul>
					<li><a href="cookies.php">Cookies</a></li>
					<li><a href="privacy-policy.php">Privacy Policy</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>

<script src="assets/js/vendor.min.js"></script>
<script src="assets/js/scripts.min.js"></script>
</body>
</html>