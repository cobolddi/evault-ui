<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
	<meta charset="UTF-8">
	<title>Evault</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="Automation of task with minification of css and js">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png">
	<link href="assets/css/vendor.min.css" rel="stylesheet">
	<link href="assets/css/styles.min.css" rel="stylesheet">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/PreloadJS/1.0.1/preloadjs.min.js"></script>
</head>

<body class="NoOverflow" id="body" >
	<div id="preloader">
		<div id="status">
			<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
				<circle class="circleSvg" cx="50" cy="50" r="45" />
			</svg>
		</div>
	</div>

	<script>
		document.addEventListener("DOMContentLoaded", init);
		let queue = new createjs.LoadQueue();
		const preload = document.querySelector('#preloader');
		const circle = preload.querySelector(".circleSvg")
		function init() {



			queue.on("progress", handleFileProgress);
			queue.on("fileload", handleFileLoad);
			queue.on("complete", handleComplete);

			queue.loadManifest([
				{ id: "video-1", src: './assets/img/new-video/video-1.mp4' },
				{ id: "image-1", src: './assets/img/evault/Home/1.jpg' },
				{ id: "image-2", src: './assets/img/evault/Home/2.jpg' },
				{ id: "image-3", src: './assets/img/evault/Home/3.jpg' }
			])
		}

		function handleFileProgress(e) {
			circle.style.strokeDashoffset = `${280 - (queue.progress * 280)}px`

		}
		function handleFileLoad(e) {
			const itemId = queue.getResult(e)

		}

		function handleComplete(e) {
			preload.style.transform = 'translateY(-100vh)';
			checkLoadedFunction()
		}

	</script>

	<header id="header" class="headerspace">
		<div class="container">
			<div class="row">
				<div class="col-6 col-md-5">
					<div class="LogoBox">
						<a href="index.php" class="NormalLogo"><img src="assets/img/logo.svg" alt="Evault"></a>
						<!-- <a href="index.php" class="WhiteLogo"><img src="assets/img/white-logo.svg" alt="Evault"></a> -->
					</div>
				</div>
				<div class="col-6 col-md-7 TillIpad">
					<!-- <span class="menu" style="float: right"><a href="">Menu</a></span> -->
					<div class="menu"
					>
						<a class="btn-open" href="#" style="color: inherit;">القائمة</a>
					</div>
				</div>
				<div class="col-8 col-md-7 IpadProOnwards">
					<div class="NavigationBlock">
						<nav>
							<ul>
								<!-- <li><a href="index.php">Home</a></li> -->
								<li><a href="how-it-work.php">يفية العمل</a></li>
								<li><a href="gallery.php">معرض الصور </a></li>
								<li><a href="faq.php">الأسئلة الشائعة</a></li>
								<li><a href="who-we-are.php">من نحن</a></li>
								<li><a href="contact-us.php">تصل بنا </a></li>
								<!-- <li class="menu "><a href="#">Menu</a></li> -->
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div class="overlay" style="background:#1C252F url(assets/img/menu-bg.png) no-repeat;">
		<div class="sub-menu">
			<div class="LogoNav">
				<a href="index.php" class="WhiteLogo"><img src="assets/img/only-white.svg" alt="Evault"></a>
				<span class="Menu"><a>Close</a></span>
			</div>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="how-it-work.php">يفية العمل</a></li>
				<li><a href="gallery.php">معرض الصور </a></li>
				<li><a href="faq.php">الأسئلة الشائعة</a></li>
				<li><a href="who-we-are.php">من نحن</a></li>
				<li><a href="contact-us.php">تصل بنا </a></li>
			</ul>
		</div>
	</div>

	<main>