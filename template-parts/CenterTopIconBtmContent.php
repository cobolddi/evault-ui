<section class="CenterTopIconBtmContent ThreeCardsBlock">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-4">
				<div class="TopIconBtmContent">
					<img src="assets/img/whatwedo.svg" alt="" class="videocontent">
					<h4 class="videocontent gs_reveal_heading">What we do?</h4>
					<p class="videocontent gs_fade_reveal">We unleash the hidden potential of
marketing campaigns, empowering
brands to reach a wider audience for an
affordable price.</p>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="TopIconBtmContent">
					<img src="assets/img/howwedo.svg" alt="" class="videocontent">
					<h4 class="videocontent gs_reveal_heading">How we do it?</h4>
					<p class="videocontent gs_fade_reveal">By providing Promotional Risk Coverage
to brands through Prize Indemnity
Insurance and Redemption Insurance.</p>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="TopIconBtmContent">
					<img src="assets/img/reallyhow.svg" alt="" class="videocontent">
					<h4 class="videocontent gs_reveal_heading">Really, how do we do it?</h4>
					<p class="videocontent gs_fade_reveal">Our service gives clients the freedom and
flexibility to plan bigger marketing
campaigns with higher rewards, with the
peace of mind that if there is a winner,
we take care of the pay-out.
</p>
				</div>
			</div>
		</div>
	</div>
</section>