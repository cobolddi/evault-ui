<section
  class="Section LogotestimonialBlock testimonial"
  data-section="whiteBody"
>
  <div class="container">
    <div class="TopHeading">
      <h2 class="gs_reveal_heading large-headings">
        <!-- Our partners in success -->
      شركائنا في النجاح


      </h2>
    </div>
    <div class="DualSliderBlock">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="logoslider">
            <div class="fourlogoslider">
              <div class="FourLogos">
                <div class="Animateblock">
                  <div class="logo">
                    <img src="assets/img/evault/Home/Audi.png" alt="" />
                  </div>
                  <div class="logo">
                    <img src="assets/img/evault/Home/Petra.png" alt="" />
                  </div>
                  <div class="logo">
                    <img
                      src="assets/img/evault/Home/Plaerrermarkt.png"
                      alt=""
                    />
                  </div>
                  <div class="logo">
                    <img src="assets/img/evault/Home/Regensburg.png" alt="" />
                  </div>
                </div>
              </div>
              <!-- <div class="FourLogos">
                <div class="Animateblock">
                  <div class="logo">
                    <img src="assets/img/evault/Home/Audi.png" alt="" />
                  </div>
                  <div class="logo">
                    <img src="assets/img/evault/Home/Petra.png" alt="" />
                  </div>
                  <div class="logo">
                    <img
                      src="assets/img/evault/Home/Plaerrermarkt.png"
                      alt=""
                    />
                  </div>
                  <div class="logo">
                    <img src="assets/img/evault/Home/Regensburg.png" alt="" />
                  </div>
                </div>
              </div> -->
              <!-- <div class="FourLogos">
                <div class="Animateblock">
                  <div class="logo">
                    <img src="assets/img/evault/Home/Audi.png" alt="" />
                  </div>
                  <div class="logo">
                    <img src="assets/img/evault/Home/Petra.png" alt="" />
                  </div>
                  <div class="logo">
                    <img
                      src="assets/img/evault/Home/Plaerrermarkt.png"
                      alt=""
                    />
                  </div>
                  <div class="logo">
                    <img src="assets/img/evault/Home/Regensburg.png" alt="" />
                  </div>
                </div>
              </div> -->
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6">
          <div class="TestimonialBlock">
            <!-- <h2 class="slideTitle large-headings">Testimonials</h2> -->
            <div class="testimonialslider">
              <div class="testimonial">
                <h4 class="GradientText slideDescription">
                 <!-- Audi -->
                 شركة أودي
                </h4>
                <p class="slideSubtitle">
                 <!-- Launched with the intention of introducing new models to customers, the 5-week E-Vault promotion
invited people to visit the Audi car dealership in Amman, Jordan and participate for a chance to win
the new Audi Car. Launched in partnership with Play 99.6 FM, the E-Vault promotion increased
footfall by 500% and sales by 100%. -->
تمثل الهدف من إطلاق عرض إيڤولت الترويجي، الذي استمر لخمسة أسابيع، في تقديم طرازات جديدة للعملاء، حيث تلقى الجمهور دعوة لحضور هذا العرض لزيارة معرض سيارات أودي في عمان بالأردن، والمشاركة للحصول على فرصة للفوز بسيارة أودي الجديدة. أطلقت فعالية العرض بالشراكة مع إذاعة "بلاي 99.6 أف أم"، حيث أسفر عرض إيڤولت الترويجي عن زيادة الإقبال بنسبة 500% وتحقيق مبيعات بنسبة 100%.
                </p>
                
              </div>
              <div class="testimonial">
                <h4 class="GradientText slideDescription">
                  <!-- Petra Insurance Brokers -->
                  شركة البتراء لوساطة التأمين
                </h4>
                <p class="slideSubtitle">
                  <!-- With a prize amount of AED 250,000 on offer, E-Vault stood out as the main attraction at the 1-day
promotional event for Petra Insurance Brokers held in Dubai. A real crowd-puller, E-Vault also
presented a great opportunity for the sales team to engage a captive audience and generate leads. -->
مع تقديم جائزة بقيمة 250,000 درهم إماراتي في العرض، برزت إيڤولت بوصفها عنصر الجذب الرئيسي للمشاركين في الفعالية الترويجية لشركة البتراء لوساطة التأمين التي أُقيمت في دبي ليوم واحد. وتمثل إيڤولت أداة حقيقة لجذب الجمهور، كما تشكل فرصة رائعة تُمكن فريق المبيعات من إشراك الجمهور المتشوق واستقطاب عملاء جدد.
                </p>
               
              </div>
              <div class="testimonial">
                <h4 class="GradientText slideDescription">
                  <!-- PLÄERRERMARKT -->
                  بلايررماركت 
                </h4>
                <p class="slideSubtitle">
               <!-- E-Vault’s promotion to launch PLÄERRERMARKT in Nuremberg, Germany was a real crowd-puller,
generating unexpectedly high footfalls, and resulted in a hugely successful launch event and opening
week for the mall. The campaign was really well received by customers. -->
لقد كانت حملة إيڤولت الترويجية لإطلاق بلايررماركت في نورمبرغ بألمانيا بمثابة أداة حقيقية لجذب الجمهور، ما أدي إلى زيادة غير متوقعة في نسبة الإقبال، وشهدت فعالية الإطلاق نجاحاً باهراً، وكذلك خلال أسبوع افتتاح مركز التسوق. وقد لقيت الحملة التسويقية استحساناً كبيراً لدى العملاء.
                </p>
              
              </div>
              <div class="testimonial">
                <h4 class="GradientText slideDescription">
               <!-- Regensburg Arcaden -->
               ريغنسبورغ أركادن
                </h4>
                <p class="slideSubtitle">
            <!-- Regensburg Arcaden’s 2-week E-Vault promotion started in Regensburg, Germany with online and
on-ground advertising. Supported by a live radio broadcast from the mall, the E-Vault promotion led
to a 30% increase in mall footfall and a 35% increase in overall mall sales. -->
دأ عرض إيڤولت الترويجي لفعالية ريغنسبورغ أركادن، الذي استمر لمدة أسبوعين في ريغنسبورغ بألمانيا من خلال الإعلانات عبر الإنترنت والإعلانات المنتشرة في كل مكان. وبدعم من البث الإذاعي المباشر من مركز التسوق، أسهم هذا العرض الترويجي في زيادة نسبة الإقبال على مركز التسوق بنسبة 30%، وزيادة في إجمالي مبيعات مركز التسوق بنسبة 35%.
                </p>
            
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="TopHeading CTAblock">
      <h2 class="gs_reveal_heading large-headings">
        <!-- The future of rewards is&nbsp;here -->
        مستقبل المكافآت هنا  
      </h2>
      <a href="contact-us.php" class="GreyBtn"
        >
        <!-- Contact Us -->
        اتصل بنا 
        <img src="assets/img/arw-right.svg" alt=""
      /></a>
    </div>
  </div>
</section>
