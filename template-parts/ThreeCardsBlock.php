<section class="ThreeCardsBlock whiteBg" data-section="whiteBody">
  <div class="OnhoverVideoBlock">
    <video width="100%" height="100%" loop preload="none" muted playsinline>
      <source src="assets/img/new-video/new-bg-video-3.mp4" type="video/mp4" />
    </video>
    <div class="IconWithHeading">
      <img src="assets/img/standout.svg" alt="" />
      <h2 class="videocontent">التميز عن الآخرين</h2>
      <p class="videocontent">
        <!-- Big prizes attract the most<br />attention and ensure<br />greater
        impact. -->
        جتذب الجوائز الكبرى عادةً أقصى قدر من الاهتمام وتضمن إحداث تأثير أكبر. 
      </p>
    </div>
  </div>
  <div class="OnhoverVideoBlock">
    <video width="100%" height="100%" loop preload="none" muted playsinline>
      <source src="assets/img/new-video/new-bg-video-3.mp4" type="video/mp4" />
    </video>
    <div class="IconWithHeading">
      <img src="assets/img/payless.svg" alt="" />
      <h2 class="videocontent">
        <!-- Pay Less -->

      تكلفة منخفضة
      </h2>
      <p class="videocontent">
        <!-- With a fixed pre-decided<br />price, always know what your<br />payout
        is. -->
        ظراً لتحديد سعر ثابت مسبقاً، ستعرف دوماً قيمة المبلغ الذي ستدفعه.

      </p>
    </div>
  </div>
  <div class="OnhoverVideoBlock">
    <video width="100%" height="100%" loop preload="none" muted playsinline>
      <source src="assets/img/new-video/new-bg-video-3.mp4" type="video/mp4" />
    </video>
    <div class="IconWithHeading">
      <img src="assets/img/feelsafe.svg" alt="" />
      <h2 class="videocontent">لشعور بالأمان</h2>
      <p class="videocontent">
        <!-- Insure your promotion and<br />avoid the financial risk of<br />prize
        redemption. -->

        تأكد من مدى ترويج علامتك التجارية وتجنب المخاطر المالية المرتبطة بتحصيل الجوائز.
      </p>
    </div>
  </div>
</section>
