<section class="CenterContentOnly">
	<div class="container">
		<div class="CenterContent">
			<h2 class="gs_reveal_heading large-headings">The finer details</h2>
			<p class="gs_fade_reveal">A brand signs up with 5th Consulting for a specific campaign. Based on the scale, goals and duration of the campaign, a fee is agreed upon. This is the starting point of your E-Vault campaign.</p>
		</div>
	</div>
</section>