<section class="TeamBlock">
  <div class="container">
    <div class="TopHeading">
      <h2 class="gs_reveal_heading large-headings">Our team</h2>
    </div>
    <div class="TeamWrapper ThreeCardsBlock">
      <div class="TeamMember">
        <img src="assets/img/team/Ralph Clemens Martin.jpg" alt="" />
        <div class="MemeberDetails">
          <h4 class="teamContent">Ralph Clemens Martin</h4>
          <h5 class="teamContent GradientText">Director</h5>
          <p class="teamContent">
            Ralph is the co-founder and CEO of EMIRAT AG Group as well as 5th
            Consulting DMCC. German by origin, Ralph graduated from Texas
            Christian University in 1990 with a specialization in Marketing and
            Business Management. He started his career in the insurance industry
            and has extensive experience working in risk management and
            marketing for multimilliondollar brands and start-ups. With a real
            understanding of the elements that drive business attention and
            conversations, Ralph has now created a reward-risk coverage
            portfolio of over USD 350 Billion per year.
          </p>
        </div>
      </div>
      <div class="TeamMember">
        <img src="assets/img/team/Andrea Bargholz.jpg" alt="" />
        <div class="MemeberDetails">
          <h4 class="teamContent">Andrea Bargholz</h4>
          <h5 class="teamContent GradientText">Director</h5>
          <p class="teamContent">
            Andrea Bargholz is the co-founder and COO of EMIRAT AG in Germany
            and CEO of 5th Consulting DMCC and 5th Insurance Advisory in Dubai,
            UAE. She started her career in Hamburg with a company that dealt
            with software for publishing houses and has performed Marketing and
            Sales roles for internet companies like Ricardo.de, QXL.de, AOL. She
            is currently overseeing the company’s ongoing expansion efforts into
            emerging markets, as a part of which, she is planning to build up
            EMIRAT Inc. in USA and 5th Consulting Ltd in UK.
          </p>
        </div>
      </div>
  
      <div class="TeamMember">
       <img src="assets/img/team/Maysa Hatahet.jpg" alt="" />
        <div class="MemeberDetails">
          <h4 class="teamContent">Maysa Hatahet</h4>
          <h5 class="teamContent GradientText">Business Manager</h5>
          <p class="teamContent">
            Passionate about marketing, advertising and events, Maysa Hatahet
            graduated with a BA degree in Business Administration and Marketing.
            Originally from Lebanon, Maysa has been living in UAE for 18 years
            and is working with 5th Consulting as a Business Development
            Manager. She is known for her strong communication and customer
            service skills and has a keen eye for assessing operational needs of
            the organization and developing solutions that reduce costs, improve
            revenue and drive customer satisfaction.
          </p>
        </div>
      </div>
       <div class="TeamMember">
        <img src="assets/img/team/Viann.jpg" alt="" />
        <div class="MemeberDetails">
          <h4 class="teamContent">Viann Alarde</h4>
          <h5 class="teamContent GradientText">Executive Assistant</h5>
          <p class="teamContent">
            Reliability, ambition, exemplary customer service and experience
            across industries has made Viann an operations rock-star. Working as
            an Executive Assistant, she remains on top of every task, including
            staff and office management, event coordination, and is always
            within reach of anyone needing any assistance at all. A strong
            business administration background and relentless determination
            makes ensures out-ofthe- box solutions and a firm grip on operations
            to ensure every task and every member is on track to achieve
            organizational goals.
          </p>
        </div>
      </div>
       <div class="TeamMember">
         <img src="assets/img/team/Yasser Satout.jpg" alt="" />
        <div class="MemeberDetails">
          <h4 class="teamContent">Yaser Satout</h4>
          <h5 class="teamContent GradientText">Sales Manager</h5>
          <p class="teamContent">
           Not your average sales professional, Yasser Satout is a problem solver and a creative thinker. He has a knack for evaluating client goals with the backdrop of market needs and coming up with the best possible solution. With over 15 years of experience in providing these solutions to clients, Yasser likes to keep abreast with the latest marketing innovations and tools and always has a success story handy to inspire and enthrall.
          </p>
        </div>
      </div>
      <div class="TeamMember">
        <img src="assets/img/team/Khurram Siddique.jpg" alt="" />
        <div class="MemeberDetails">
          <h4 class="teamContent">Khurram Siddique</h4>
          <h5 class="teamContent GradientText">Sales Manager</h5>
          <p class="teamContent">
            Tenacious and Effective Sales Management professional and business
            developer, with strong technical aptitude. Trusted leader with
            proven history of providing successful solutions for clients
            toughest business challenges. Khurram is involved in all aspects of
            the company’s sales and marketing operations. He is passionate about
            insurance and finance, having over 14 years of extensive GCC
            experience as Sales, Insurance &amp; Risk Consultancy. Having
            graduated with a bachelor’s in business administration, and
            specializing in Finance, Khurram also has a diploma in General
            insurance management with a CII certification, and has a “serving,
            not selling,” approach to work that helps him serve and empower his
            clients.
          </p>
        </div>
      </div>
     
    
    </div>
  </div>
</section>

<!-- 
  <div class="TeamMember">
        <img src="assets/img/team/Maysa Hatahet.jpg" alt="" />
        <div class="MemeberDetails">
          <h4 class="teamContent">Maysa Hatahet</h4>
          <h5 class="teamContent GradientText">Business Development Manager</h5>
          <p class="teamContent">
            Passionate about marketing, advertising and events, Maysa Hatahet
            graduated with a degree in Business Administration and Marketing.
        <img src="assets/img/team/Maysa Hatahet.jpg" alt="" />
        <div class="MemeberDetails">
          <h4 class="teamContent">Maysa Hatahet</h4>
          <h5 class="teamContent GradientText">Business Manager</h5>
          <p class="teamContent">
            Passionate about marketing, advertising and events, Maysa Hatahet
            graduated with a BA degree in Business Administration and Marketing.
            Originally from Lebanon, Maysa has been living in UAE for 18 years
            and is working with 5th Consulting as a Business Development
            Manager. She is known for her strong communication and customer
            service skills and has a keen eye for assessing operational needs of
            the organization and developing solutions that reduce costs, improve
            revenue and drive customer satisfaction.
          </p>
        </div>
      </div>
      <div class="TeamMember">
        <img src="assets/img/team/Khurram Siddique.jpg" alt="" />
        <div class="MemeberDetails">
          <h4 class="teamContent">Khurram Siddique</h4>
          <h5 class="teamContent GradientText">Sales Manager</h5>
          <p class="teamContent">
            As a sales manager in 5th Consulting DMCC, Khurram Siddique is
            involved in all aspects of the company’s sales and marketing
            operations. He is passionate about insurance and finance and has
            experience working in the Kingdom of Saudi Arabia as an Insurance &
            Risk Consultant at SAMASCO & Kingdom Hospital for 14 years. Having
            Tenacious and Effective Sales Management professional and business
            developer, with strong technical aptitude. Trusted leader with
            proven history of providing successful solutions for clients
            toughest business challenges. Khurram is involved in all aspects of
            the company’s sales and marketing operations. He is passionate about
            insurance and finance, having over 14 years of extensive GCC
            experience as Sales, Insurance &amp; Risk Consultancy. Having
            graduated with a bachelor’s in business administration, and
            specializing in Finance, Khurram also has a diploma in General
            insurance management with a CII certification, and has a “serving,
            not selling,” approach to work that helps him serve and empower his
            clients.
          </p>
        </div>
      </div>
      <div class="TeamMember">
        <img src="assets/img/team/Yasser Satout.jpg" alt="" />
        <div class="MemeberDetails">
          <h4 class="teamContent">Yasser Satout</h4>
          <h4 class="teamContent">Yaser Satout</h4>
          <h5 class="teamContent GradientText">Sales Manager</h5>
          <p class="teamContent">
            Not your average sales professional, Yasser Satout is a problem
            solver and a creative thinker. He has a knack for evaluating client
            goals with the backdrop of market needs and coming up with the best
            possible solution. With over 15 years of experience in providing
            these solutions to clients, Yasser likes to keep abreast with the
            latest marketing innovations and tools and always has a success
            story handy to inspire and enthrall.
          </p>
        </div>
      </div>
      <div class="TeamMember">
        <img src="assets/img/team/Viann.jpg" alt="" />
        <div class="MemeberDetails">
          <h4 class="teamContent">Viann Alarde</h4>
          <h5 class="teamContent GradientText">Executive Assistant</h5>
          <p class="teamContent">
            Reliability, ambition, exemplary customer service and experience
            across industries has made Viann an operations rock-star. Working as
            an Executive Assistant, she remains on top of every task, including
            staff and office management, event coordination, and is always
            within reach of anyone needing any assistance at all. A strong
            business administration background and relentless determination
            makes ensures out-ofthe- box solutions and a firm grip on operations
            to ensure every task and every member is on track to achieve
            organizational goals.
          </p>
        </div>
      </div> -->