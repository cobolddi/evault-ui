<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>Evault</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta
      name="description"
      content="Automation of task with minification of css and js"
    />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png" />

    <link href="assets/css/vendor.min.css" rel="stylesheet" />
    <link href="assets/css/styles.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/PreloadJS/1.0.1/preloadjs.min.js"></script>
  </head>

  <body class="NoOverflow" id="body">
    <header id="header" class="headerspace NormalHeader">
      <div class="container">
        <div class="row">
          <div class="col-6 col-md-5">
            <div class="LogoBox">
              <a href="index.php" class="NormalLogo"
                ><img src="assets/img/logo.svg" alt="Evault"
              /></a>
              <!-- <a href="index.php" class="WhiteLogo"><img src="assets/img/white-logo.svg" alt="Evault"></a> -->
            </div>
          </div>
          <div class="col-6 col-md-7 TillIpad">
            <!-- <span class="menu" style="float: right"><a href="">Menu</a></span> -->
            <div
              class="menu"
              style="
                display: flex;
                align-items: center;
                justify-content: flex-end;
                font-weight: 600;
              "
            >
              <a class="btn-open" href="#">Menu</a>
            </div>
          </div>
          <div class="col-8 col-md-7 IpadProOnwards">
            <div class="NavigationBlock">
              <nav>
                <ul>
                  <!-- <li><a href="index.php">Home</a></li> -->
                  <li><a href="how-it-work.php">How it works</a></li>
                  <li><a href="gallery.php">Gallery</a></li>
                  <li><a href="faq.php">FAQ</a></li>
                  <li><a href="who-we-are.php">Who we are</a></li>
                  <li><a href="contact-us.php">Contact</a></li>
                  <!-- <li class="menu "><a href="#">Menu</a></li> -->
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div
      class="overlay"
      style="background: url(assets/img/menu-bg.png) no-repeat"
    >
      <div class="sub-menu">
        <div class="LogoNav">
          <a href="index.php" class="WhiteLogo"
            ><img src="assets/img/only-white.svg" alt="Evault"
          /></a>
          <span class="Menu"><a>Close</a></span>
        </div>
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="how-it-work.php">How it works</a></li>
          <li><a href="gallery.php">Gallery</a></li>
          <li><a href="faq.php">FAQ</a></li>
          <li><a href="who-we-are.php">Who we are</a></li>
          <li><a href="contact-us.php">Contact</a></li>
        </ul>
      </div>
    </div>

    <main>
      <div class="container">
        <div class="general_content">
          <div
            class="vc_row wpb_row section vc_row-fluid"
            style="text-align: left"
          >
            <div class="full_section_inner clearfix">
              <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element">
                    <h2>Privacy Policy</h2>
                      <div class="wpb_wrapper">
                        <h3>
                          5th Consulting, its affiliates or subsidiaries (<b
                            >”5th Consulting”, “our”, “we” or “us”</b
                          >) wants you to be familiar with how we collect, use
                          and disclose Personal Data. This Privacy Notice is
                          meant to help you understand our privacy practices,
                          including what Personal Data we collect, why we
                          collect it, what we do with it, and how we protect it,
                          as well as your individual rights.
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            class="vc_row wpb_row section vc_row-fluid"
            style="text-align: left"
          >
            <div class="full_section_inner clearfix">
              <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner vc_custom_1536901834014">
                  <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h3>Introduction</h3>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <div
                          class="p-vertical-positioning p-text-container p-valign-top"
                        >
                          <div class="p-text-container-inner">
                            <div
                              class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                            >
                              5th Consulting appreciates the interest you have
                              shown in our company, products and services by
                              visiting our websites (<a href="http://evault.info/">evault.info</a>) or related
                              communication channels, including, but not limited
                              to, our social media pages and/or channels and
                              blogs (together “5th Consulting Pages”).<u
                              > </u>This Privacy Notice applies to the Personal
                              Data that we collect or process when you interact
                              with 5th Consulting Pages. You can interact with
                              5th Consulting through 5th Consulting Pages,
                              through the purchase and use of our
                              products/services, through marketing
                              communications, as well as through our customer
                              support. These interactions can be made by
                              visitors, customers, business customers, suppliers
                              and/or business partners. Your privacy is of
                              sincere importance to us. You have shown your
                              trust in us by interacting with our 5th Consulting
                              Pages and we value that trust. This means that we
                              are committed to protecting your privacy at all
                              times.
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            class="vc_row wpb_row section vc_row-fluid"
            style="text-align: left"
          >
            <div class="full_section_inner clearfix">
              <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            class="vc_row wpb_row section vc_row-fluid"
            style="text-align: left"
          >
            <div class="full_section_inner clearfix">
              <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner vc_custom_1536901834014">
                  <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h3>
                          <span
                            class="p-heading-03 p-heading-light p-heading-secondary"
                            >What personal data do we collect and for which
                            purpose?</span
                          >
                        </h3>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <div
                          class="p-vertical-positioning p-text-container p-valign-top"
                        >
                          <div class="p-text-container-inner">
                            <div
                              class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                            >
                              When using the term “Personal Data” in our Privacy
                              Notice, we mean information that relates to you
                              and allows us to identify you, either directly or
                              in combination with other information that we may
                              hold.
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >5th Consulting Account Data</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          <p>
                            You can chose to create an online 5th Consulting
                            Account to benefit from our services and online
                            shopping experience.
                          </p>
                          <div class="p-show-more-content">
                            <p>
                              When you create a 5th Consulting Account we
                              collect the following Personal Data:
                            </p>
                            <ol class="p-numbers">
                              <li>Name;</li>
                              <li>Date of birth or age;</li>
                              <li>Salutation;</li>
                              <li>Email address;</li>
                              <li>Password;</li>
                              <li>
                                Country&nbsp;and language preference determined
                                by the extended url from the 5th Consulting page
                                you create your account;
                              </li>
                              <li>
                                If you decided to create a 5th Consulting
                                Account with your social media account, we may
                                receive the social media profile details you
                                choose to share with&nbsp;us;
                              </li>
                              <li>
                                Furthermore, we may need the following
                                information when you interact with 5th
                                Consulting as a Professional (i.e. business
                                customer, supplier and/or business partner):
                              </li>
                            </ol>
                            <p>
                              ○ Company name ○ Company address ○ Company phone
                              number ○ Organization Type ○ Job title/specialty ○
                              Customer Account number We use this Personal Data
                              in order to create and maintain your 5th
                              Consulting Account. You can use your 5th
                              Consulting Account for various services. ‘When
                              using your 5th Consulting Account for a service,
                              we may add additional Personal Data to your 5th
                              Consulting Account. The following paragraphs
                              inform you of the services you may use and what
                              Personal Data we will add to your 5th Consulting
                              Account when you do. Your 5th Consulting Account
                              creation only happens at your request. Therefore,
                              we consider the processing of your Account Data to
                              be necessary for the performance of the contract
                              to which you are party and lawful.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >Purchase &amp; Registration Data</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          <div class="p-show-more-content">
                            <p>
                              When you purchase products/services, and when your
                              register your product, we collect the following
                              Personal Data:
                            </p>
                            <ul class="p-bullets">
                              <li>Shipment and invoice address;</li>
                              <li>Phone number;</li>
                              <li>
                                Invoice history, which includes an overview of
                                5th Consulting products/services purchased;
                              </li>
                              <li>
                                Details of conversations that you may have with
                                the Customer Care departments around your
                                purchase questions or online experience
                                questions;
                              </li>
                              <li>
                                Details of your registered Product/service, such
                                as the name of the product/service, the product
                                category it belongs to, the product model
                                number, date of purchase, proof of purchase.
                              </li>
                            </ul>
                            <p>
                              Purchase and registration of products only happens
                              at your request. We consider the processing of
                              your Purchase and Registration Data to be
                              necessary for the performance of the contract to
                              which you are party and lawful
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >Promotional Communications Data</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          <p>
                            You can choose to sign up for marketing and
                            promotional communications.
                          </p>
                          <div class="p-show-more-content">
                            <p>
                              When you sign up for marketing and promotional
                              communications, we will use the following Personal
                              Data:
                            </p>
                            <ul class="p-bullets">
                              <li>Email address;</li>
                              <li>Your 5th Consulting Account Data;</li>
                              <li>Date of birth or age;</li>
                              <li>
                                Your interactions with 5th Consulting Pages, 5th
                                Consulting mobile apps, the emails you clicked
                                and opened and attendance of events.
                              </li>
                            </ul>
                            <p>
                              We use this Personal Data to send you promotional
                              communications – based on your preferences and
                              behavior – about 5th Consulting products,
                              services, events and promotions. 5th Consulting
                              group companies may contact you with promotional
                              communications via email, SMS and other digital
                              channels, such as mobile apps and social media. To
                              be able to tailor the communications to your
                              preferences and behavior and provide you with the
                              best, personalized experience, we may analyze and
                              combine all information associated with your
                              Account Data and data about your interactions with
                              5th Consulting. We also use this information to
                              create segments of our audience showing which of
                              our products and/or services users are interested
                              in and track success of our marketing efforts. 5th
                              Consulting will give you the opportunity to
                              withdraw your consent for receiving promotional
                              communications at any time via the unsubscribe
                              link at the bottom of each promotional
                              communications email you may receive from us. We
                              may also send 5th Consulting press releases by
                              email (Press email data). When 5th Consulting
                              sends press releases, 5th Consulting will process
                              your name, contact details (e.g. email) and
                              company information. The processing of Personal
                              Data for Promotional Communications are based on
                              your consent, i.e. provided on a voluntary basis;
                              such basis is lawful. Where 5th Consulting
                              processes your Personal Data based on consent, you
                              have the right to withdraw your consent at any
                              time, without affecting the lawfulness of
                              processing based on consent before withdrawing
                              your consent
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >Competition Data</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          <p>
                            You may choose to participate in contest games or
                            other competitions (Competition) via our 5th
                            Consulting Pages.
                          </p>
                          <div class="p-show-more-content">
                            <p>
                              When you sign up for a Competition, we will use
                              the following Personal Data:
                            </p>
                            <ul class="p-bullets">
                              <li>
                                Your Account Data in case you participate after
                                logging in to your 5th Consulting account;
                              </li>
                              <li>
                                Your social media profile data in case you
                                participate via your social media account;
                              </li>
                              <li>Name;</li>
                              <li>Email address;</li>
                              <li>Telephone number;</li>
                              <li>Physical address;</li>
                              <li>Date of birth or age,;</li>
                              <li>
                                Material or content generated and provided by
                                you that allows you to participate in the
                                sweepstake, contest, game or other competition.
                              </li>
                            </ul>
                            <p>
                              We use this Personal Data to allow you to
                              participate, to identify you, to prevent/detect
                              fraud and to fulfill the prize to you (if you
                              win). We consider the processing of your
                              Competition Data in the above context to be
                              necessary for the performance of the contract to
                              which you are party and lawful
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >Online Feedback Data</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          <p>
                            You can choose to give your feedback, comments,
                            questions, ratings and reviews in online evaluations
                            (Online Feedback).
                          </p>
                          <div class="p-show-more-content">
                            <p>
                              Where you make use of this opportunity, we will
                              process the following Personal Data:
                            </p>
                            <ul class="p-bullets">
                              <li>Your cookie ID;</li>
                              <li>
                                Your feedback/comments/questions/your rating
                                review;
                              </li>
                              <li>
                                Your contact details in case you provide them to
                                us (name, email, telephone number).
                              </li>
                            </ul>
                            <p>
                              We use this Personal Data to provide 5th
                              Consulting with valuable insights to improve our
                              5th Consulting Pages or products/services range.
                              We consider the processing of your Online Feedback
                              Data to be based on legitimate interest of 5th
                              Consulting to improve our 5th Consulting pages and
                              our range of products and services and to be
                              lawful.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b><span class="p-heading-04">Events Data</span></b>
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          <p>
                            5th Consulting may collect data for events, webinars
                            or fairs (“Events”) that you may wish to attend and
                            register for via the 5th Consulting Pages. You may
                            either register for the Event via 5th Consulting
                            Pages or via the other organizing company.
                          </p>
                          <div class="p-show-more-content">
                            <p>
                              When you register for an event, we will process
                              the following information:
                            </p>
                            <ul class="p-bullets">
                              <li>Account Data;</li>
                              <li>Name;</li>
                              <li>Email;</li>
                              <li>Country;</li>
                              <li>Salutation;</li>
                              <li>Product/service interest.</li>
                            </ul>
                            <p>
                              By registering to an event, you are agreeing to
                              receive communications from 5th Consulting
                              directly related to the event, such as what
                              location the event will be hosted, what time the
                              event takes place. We consider the processing of
                              Events Data to be necessary for the performance of
                              the contract to which you are party and lawful.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >Social listening Data</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          <p>
                            When you interact or communicate on social media
                            channels/pages/promotions and blogs (e.g. when you
                            click on ‘like’ or ‘share’, when you post and share
                            comments and when you submit ratings and reviews)
                            your Personal Data is processed by a third party
                            that provides social listening services to 5th
                            Consulting. This means that the third party that
                            provides social listening services will retain a
                            copy of your online publicly available interactions
                            to which the third party has provided us access.
                          </p>
                          <div class="p-show-more-content">
                            The Personal Data we collect includes publically
                            available data provided in the social media context
                            by you from the relevant social media provider via a
                            third party that provides social listening services
                            such as: name, gender, birthday or age, homepage,
                            profile photo, time zone, mail address, country,
                            interests, and comments and content you have
                            posted/shared. We use this Personal Data to gain a
                            general view of the opinion of people about us and
                            our brands and to get an idea of relevant online
                            influencers and to resolve issues and/or improve 5th
                            Consulting products and services and/or start a
                            promotional conversation with you (based on
                            questions/requests that you addressed to us or our
                            competitors). We consider the processing of Social
                            Listening Data in the above context to be based on a
                            legitimate interest of 5th Consulting to be lawful.
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >Consumer or Customer Care Data</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          <p>
                            You may interact with our Consumer or Customer Care
                            centers.
                          </p>
                          <div class="p-show-more-content">
                            <p>
                              When you contact Customer Support, or when you
                              communicate with one of our 5th Consulting
                              Customer Care representatives by email, phone or
                              in writing (e.g. reply card) we will use the
                              following Personal Data:
                            </p>
                            <ul class="p-bullets">
                              <li>Your Account data;</li>
                              <li>
                                Your call recording and history, purchase
                                history, the content of your questions, or
                                requests that you addressed.
                              </li>
                            </ul>
                            <p>
                              We use this Personal Data to provide you with
                              consumer and customer support (which may be
                              product- or service related), respond to your
                              inquiries, fulfil your requests and provide you
                              with related customer service, such as repair or
                              redeem products/services. We also use this
                              Personal Data to improve, fix and customize our
                              products and services, to meet compliance
                              standards and to educate our Customer Care
                              employees during training. If we provide this
                              service on the basis of an agreement with you, we
                              consider the processing of your Personal Data to
                              be necessary for the performance of the contract
                              to which you are party and lawful. In other cases,
                              we consider the processing of your Personal Data
                              in the above context to be based on a legitimate
                              interest of 5th Consulting to be lawful.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >Improve our products and service (Improvement
                              Data)</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          <p>
                            Where we use Personal Data we have collected from
                            you while you have been using 5th Consulting
                            Products/Services and/or our 5th Consulting Pages
                            for analytical purposes, we do this in order to
                            improve our Products/Services and to enhance your
                            user experience. We also do this in order to have a
                            general overview of the category of audiences that
                            are interested in which of our Products and/or
                            Services.
                          </p>
                          <div class="p-show-more-content">
                            We consider the processing of your Personal Data in
                            the above context to be based on legitimate interest
                            of 5th Consulting to improve our 5th Consulting
                            pages and our range of products and services and to
                            be lawful.
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h3>
                          <span
                            class="p-heading-03 p-heading-light p-heading-secondary"
                            >Sharing your Personal Data with others</span
                          >
                        </h3>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >Affiliates and service providers</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          <p>We may disclose your Personal Data:</p>
                          <div class="p-show-more-content">
                            <ul class="p-bullets">
                              <li>
                                To our affiliates or subsidiaries for the
                                purposes described in this Privacy Notice;
                              </li>
                              <li>
                                To our third party service providers who provide
                                services such as website hosting, data analysis,
                                payment/billing processing, order fulfillment,
                                information technology and related
                                infrastructure provision, customer service,
                                email delivery, CRM, identity management, event
                                management, marketing intelligence, auditing,
                                fraud detection and other services;
                              </li>
                              <li>
                                To third parties, to permit them to send you
                                marketing communications, consistent with your
                                choices if you have opted into such sharing;
                              </li>
                              <li>
                                To third-party sponsors of sweepstakes, contests
                                and similar promotions;
                              </li>
                              <li>
                                To your connections associated with your social
                                media account, to other website users and to
                                your social media account provider, in
                                connection with your social sharing activity. By
                                connecting your 5th Consulting account and your
                                social media account, you authorize us to share
                                information with your social media account
                                provider, and you understand that the use of the
                                information we share is governed by the social
                                media provider’s privacy policy;
                              </li>
                              <li>
                                When you interact with 5th Consulting as a
                                business customer, supplier or business partner,
                                5th Consulting may disclose Personal Data
                                available in reports and other materials
                                provided by you and/or your company pursuant to
                                an engagement with another business partner
                                and/or a supplier.
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >Other uses and disclosures</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          <p>
                            We may also use and disclose your Personal Data as
                            we believe to be necessary or appropriate: (a) to
                            comply with applicable law, which may include laws
                            outside your country of residence, to respond to
                            requests from public and government authorities,
                            which may include authorities outside your country
                            of residence, to cooperate with law enforcement or
                            for other legal reasons; (b) to enforce our terms
                            and conditions; and (c) to protect our rights,
                            privacy, safety or property, and/or that of our
                            affiliates or subsidiaries, you or others.
                          </p>
                          <div class="p-show-more-content">
                            In addition, we may use, disclose or transfer your
                            information to a third party in the event of any
                            reorganization, merger, sale, joint venture,
                            assignment, transfer or other disposition of all or
                            any portion of our business, assets or stock
                            (including in connection with any bankruptcy or
                            similar proceedings).
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >Third-party services</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          <p>
                            This Privacy Notice does not address, and we are not
                            responsible for, the privacy, information or other
                            practices of any third parties, including any third
                            party operating any website or service to which the
                            5th Consulting Pages link. The inclusion of a link
                            on the 5th Consulting Pages does not imply
                            endorsement of the linked site or service by us or
                            by our affiliates or subsidiaries.
                          </p>
                          <div class="p-show-more-content">
                            In addition, we are not responsible for the
                            information collection, use, disclosure or security
                            policies or practices of other organizations, such
                            as Facebook, Twitter, Apple, Google, Microsoft, RIM
                            or any other app developer, app provider, social
                            media platform provider, operating system provider,
                            wireless service provider or device manufacturer,
                            including with respect to any Personal Data you
                            disclose to other organizations through or in
                            connection with the 5th Consulting Pages. These
                            other organizations may have their own privacy
                            notices, statements or policies. We strongly suggest
                            that you review them to understand how your Personal
                            Data may be processed by those other organizations.
                            By connecting your 5th Consulting account and your
                            social media account, you authorize us to share
                            information with your social media account provider,
                            and you understand that the use of the information
                            we share will be governed by the social media
                            provider’s privacy policy. Your Personal Data may be
                            shared with your friends associated with your social
                            media account, to other website users and to your
                            social media account provider, in connection with
                            your social sharing activity. 5th Consulting is
                            bound by the privacy practices or policies of these
                            third parties as well as our own.
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >Third-party cookies or other tracking
                              technologies</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          <p>
                            We use cookies or similar technologies when you
                            interact with and use the 5th Consulting Pages –
                            when you visit our websites, use our social media
                            pages, receive our emails, use our mobile apps
                            and/or connected devices. In most cases we will not
                            be able to identify you directly from the
                            information we collect using these technologies.
                          </p>
                          <div class="p-show-more-content">
                            <p>The data collected is used to:</p>
                            <ul class="p-bullets">
                              <li>
                                Ensure that the digital channel functions
                                properly;
                              </li>
                              <li>
                                Analyze digital channel usage so we can measure
                                and improve performance of our digital channels;
                              </li>
                              <li>
                                Connect to your social media networks and read
                                reviews;
                              </li>
                              <li>
                                Help better tailor advertising to your
                                interests, both within and beyond 5th Consulting
                                digital channels;
                              </li>
                            </ul>
                            <p>
                              You can delete cookies if you wish; while certain
                              cookies are necessary for viewing and navigating
                              on our website or app, most of the features will
                              be still accessible without cookies. For further
                              information about the use of cookies or other
                              similar technologies used and your choices
                              regarding cookies, please read our
                              <a
                                href="cookies.php"
                                target="_blank"
                                rel="noopener noreferrer"
                                data-is-internal="true"
                                >Cookie Notice</a
                              >.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h3>How we protect your Personal Data</h3>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >How do we secure your Personal Data?</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          We seek to use reasonable organizational, technical
                          and administrative measures to protect Personal Data
                          within our organization. Unfortunately, no data
                          transmission or storage system can be guaranteed to be
                          100% secure. If you have reason to believe that your
                          interaction with us is no longer secure, please
                          immediately notify us in accordance with the
                          <i>“How can you contact us?”</i> section below.
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <div
                          class="p-vertical-positioning p-text-container p-valign-top"
                        >
                          <div class="p-text-container-inner">
                            <h4>
                              <b
                                ><span class="p-heading-04"
                                  >How long will the Personal Data be
                                  kept?</span
                                ></b
                              >
                            </h4>
                            <div
                              class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                            >
                              We will retain your Personal Data for as long as
                              needed or permitted in light of the purpose(s) for
                              which it was obtained. The criteria used to
                              determine our retention periods include: (i) the
                              length of time we have an ongoing relationship
                              with you to participate in the 5th Consulting
                              pages; (ii) whether there is a legal obligation to
                              which we are subject; or (iii) whether retention
                              is advisable in light of our legal position (such
                              as in regard to applicable statutes of
                              limitations, litigation or regulatory
                              investigations).
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >Jurisdiction and cross-border transfer</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          Your Personal Data may be stored and processed in any
                          country where we have facilities or in which we engage
                          service providers, and by using the 5th Consulting
                          Pages you consent to the transfer of information to
                          countries outside of your country of residence, which
                          may have data protection rules that are different from
                          those of your country. In certain circumstances,
                          courts, law enforcement agencies, regulatory agencies
                          or security authorities in those other countries may
                          be entitled to access your Personal Data.
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h3>
                          <span
                            class="p-heading-03 p-heading-light p-heading-secondary"
                            >Rights and choices available to you</span
                          >
                        </h3>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h4>
                          <b
                            ><span class="p-heading-04"
                              >Your rights and choices available to you</span
                            ></b
                          >
                        </h4>
                        <div
                          class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                        >
                          If you would like to submit a request to access,
                          rectify, erase, restrict or object to the processing
                          of Personal Data that you have previously provided to
                          us, or if you would like to submit a request to
                          receive an electronic copy of your Personal Data for
                          purposes of transmitting it to another company (to the
                          extent this right to data portability is provided to
                          you by applicable law), you may contact us via our
                          <a href="contact-us.php" data-is-internal="true"
                            >contact form</a
                          >. We will respond to your request consistent with
                          applicable law. In your request, please make clear
                          what Personal Data you would like to have changed,
                          whether you would like to have your Personal Data
                          suppressed from our database, or otherwise let us know
                          what limitations you would like to put on our use of
                          your Personal Data. For your protection, we may only
                          implement requests with respect to the Personal Data
                          associated with your account, your email address or
                          other account information, that you use to send us
                          your request, and we may need to verify your identity
                          before implementing your request. You may also opt-out
                          from receiving electronic communications from 5th
                          Consulting. If you no longer want to receive
                          marketing-related emails on a going-forward basis, you
                          may opt-out by clicking the unsubscribe button at the
                          bottom of each promotional email you receive from us.
                          Please note that we may need to retain certain
                          information for recordkeeping purposes and/or to
                          complete any transactions that you began prior to
                          requesting a change or deletion (e.g. when you make a
                          purchase or enter a promotion, you may not be able to
                          change or delete the Personal Data provided until
                          after the completing of such a purchase or promotion).
                          There may also be residual information that will
                          remain within our databases and other records, which
                          will not be removed. We will try to comply with your
                          request(s) as soon as reasonably practicable. Please
                          note that if you opt-out of receiving
                          marketing-related emails from us, we may still send
                          you important administrative messages, from which you
                          cannot opt-out. If you make use of (some of) your
                          choices and rights, you may not be able to use, in
                          whole or in part, certain 5th Consulting Services.
                        </div>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h3>
                          <span
                            class="p-heading-03 p-heading-light p-heading-secondary"
                            >Special information for parents</span
                          >
                        </h3>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <p>
                          The 5th Consulting Pages are not directed to children,
                          as defined under applicable law, and we do not
                          knowingly collect Personal Data from children. It is,
                          however, 5th Consulting policy to comply with the law
                          when it requires parent or guardian permission before
                          collecting, using or disclosing Personal Data of
                          children. We are committed to protecting the privacy
                          needs of children and we strongly encourage parents
                          and guardians to take an active role in their
                          children’s online activities and interests. If a
                          parent or guardian becomes aware that his or her child
                          has provided us with his or her Personal Data without
                          their consent, please contact us via our
                          <a href="contact-us.php" data-is-internal="true"
                            >contact form</a
                          >. If we become aware that a child has provided us
                          with Personal Data, we will delete his/her data from
                          our files.
                        </p>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <h3>
                          <span
                            class="p-heading-03 p-heading-light p-heading-secondary"
                            >How can you contact us?</span
                          >
                        </h3>
                      </div>
                    </div>

                    <div class="wpb_text_column wpb_content_element">
                      <div class="wpb_wrapper">
                        <div class="gc11v3-generictext component-base section">
                          <section
                            class="p-gc11v3-generictext p-valign-parent p-row-gutter p-l-spacing-top-a p-m-spacing-top-a p-s-spacing-top-a p-xs-spacing-top-a"
                            data-comp-id="gc11v3Generictext"
                            data-containerlink='{"useLink":"no", "url":"", "navdest":"", "target":"_top", "omniture":""}'
                            data-is-internal="true"
                          >
                            <div
                              class="p-vertical-positioning p-text-container p-valign-top"
                            >
                              <div class="p-text-container-inner">
                                <div
                                  class="p-body-text p-text-smaller p-l-col1 p-m-col1 p-s-col1 p-xs-col1"
                                >
                                  If you have any questions about this Privacy
                                  Notice or about the way 5th Consulting uses
                                  your Personal Data, please contact our
                                  marketing via our
                                  <a
                                    href="contact-us.php"
                                    data-is-internal="true"
                                    >contact form</a
                                  >.
                                </div>
                              </div>
                            </div>
                          </section>
                        </div>
                      </div>
                    </div>
                    <div class="vc_empty_space" style="height: 64px">
                      <span class="vc_empty_space_inner">
                        <span class="empty_space_image"></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>

    <?php @include('template-parts/footer.php') ?>
  </body>
</html>
