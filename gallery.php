<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>Evault</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta
      name="description"
      content="Automation of task with minification of css and js"
    />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png" />

    <link href="assets/css/vendor.min.css" rel="stylesheet" />
    <link href="assets/css/styles.min.css" rel="stylesheet" />
  </head>
  <body class="NoOverflow rtl" id="body" >
    <!-- <div id="preloader"> 
  <div id="status">
  	<img src="assets/img/loader.gif" alt="Loader">
  </div> 
</div> -->

    <header id="header" class="headerspace NormalHeader">
      <div class="container">
        <div class="row">
          <div class="col-6 col-md-5">
            <div class="LogoBox">
              <a href="index.php" class="NormalLogo"
                ><img src="assets/img/logo.svg" alt="Evault"
              /></a>
              <!-- <a href="index.php" class="WhiteLogo"><img src="assets/img/white-logo.svg" alt="Evault"></a> -->
            </div>
          </div>
          <div class="col-6 col-md-7 TillIpad">
            <!-- <span class="menu" style="float: right"><a href="">Menu</a></span> -->
            <div
              class="menu"
            
            >
              <a class="btn-open" href="#">Menu</a>
            </div>
          </div>
          <div class="col-8 col-md-7 IpadProOnwards">
            <div class="NavigationBlock">
              <nav>
                <ul>
                  <!-- <li><a href="index.php">Home</a></li> -->
                  <li><a href="how-it-work.php">How it works</a></li>
                  <li><a href="gallery.php">Gallery</a></li>
                  <li><a href="faq.php">FAQ</a></li>
                  <li><a href="who-we-are.php">Who we are</a></li>
                  <li><a href="contact-us.php">Contact</a></li>
                  <!-- <li class="menu "><a href="#">Menu</a></li> -->
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div
      class="overlay"
      style="background: url(assets/img/menu-bg.png) no-repeat"
    >
      <div class="sub-menu">
        <div class="LogoNav">
          <a href="index.php" class="WhiteLogo"
            ><img src="assets/img/only-white.svg" alt="Evault"
          /></a>
          <span class="Menu"><a>Close</a></span>
        </div>
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="how-it-work.php">How it works</a></li>
          <li><a href="gallery.php">Gallery</a></li>
          <li><a href="faq.php">FAQ</a></li>
          <li><a href="who-we-are.php">Who we are</a></li>
          <li><a href="contact-us.php">Contact</a></li>
        </ul>
      </div>
    </div>

    <main>
      <section class="Gallery">
        <div class="container">
          <div class="row">
            <div class="col-12 col-md-4 galleryHeading">
              <h2 class="gs_reveal_heading large-headings mb1-6">Gallery</h2>
              <h4 class="GradientText mb1-6">
                Every successful brand<br />
                story is a picture.
              </h4>
              <p>
                Browse through our gallery to see<br />
                glimpses of the many beautiful pictures<br />
                painted with the help of the E-Vault.
              </p>
            </div>
            <div class="col-12 col-md-8">
              <div class="GalleryTabs">
                <div class="buttons">
                  <a class="tab-links active" onclick="openTab(event, 'first')"
                    >All</a
                  >
                  <a class="tab-links" onclick="openTab(event, 'second')"
                    >Photos</a
                  >
                  <a class="tab-links" onclick="openTab(event, 'third')"
                    >Videos</a
                  >
                </div>

                <div id="first" class="tab-content">
                  <div class="flexbox" id="gallery">
                    <a
                      href="assets/img/gallery/01.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/01.jpg" alt=""
                    /></a>
                    <a
                      href="assets/img/gallery/02.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/02.jpg" alt=""
                    /></a>
                    
                    <a
                      href="assets/img/gallery/04.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/04.jpg" alt=""
                    /></a>

                    <a
                      href="assets/img/gallery/05.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/05.jpg" alt=""
                    /></a>
                    <a
                      href="assets/img/gallery/08.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/08.jpg" alt=""
                    /></a>
                      <a
                      href="assets/img/gallery/12.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/12.jpg" alt=""
                    /></a>
                    <a
                      href="assets/img/gallery/03.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/03.jpg" alt=""
                    /></a>
                    <a
                      href="assets/img/gallery/06.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/06.jpg" alt=""
                    /></a>
                    <a
                      href="assets/img/gallery/07.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/07.jpg" alt=""
                    /></a>
                    
                    <a
                      href="assets/img/gallery/09.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/09.jpg" alt=""
                    /></a>

                    <a
                      href="assets/img/gallery/10.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/10.jpg" alt=""
                    /></a>
                    <a
                      href="assets/img/gallery/11.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/11.jpg" alt=""
                    /></a>
                  
                    <!-- <a
                      href="assets/img/gallery/13.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/13.jpg" alt=""
                    /></a> -->
                    <!-- <a
                      href="assets/img/gallery/14.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/14.jpg" alt=""
                    /></a> -->

                    <a href="#video-01" class="openVideo item">
                      <img src="assets/img/gallery/video-1-poster.png" alt="" />
                    </a>
                    <a href="#video-02" class="openVideo item">
                      <img src="assets/img/gallery/video-2-poster.png" alt="" />
                    </a>
                    <a href="#video-03" class="openVideo item">
                      <img src="assets/img/gallery/video-3-poster.png" alt="" />
                    </a>
                  </div>
                </div>

                <div id="second" class="tab-content">
                  <div class="flexbox" id="gallery">
                    <a
                      href="assets/img/gallery/01.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/01.jpg" alt=""
                    /></a>
                    <a
                      href="assets/img/gallery/02.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/02.jpg" alt=""
                    /></a>
                    
                    <a
                      href="assets/img/gallery/04.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/04.jpg" alt=""
                    /></a>

                    <a
                      href="assets/img/gallery/05.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/05.jpg" alt=""
                    /></a>
                    <a
                      href="assets/img/gallery/08.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/08.jpg" alt=""
                    /></a>
                     <a
                      href="assets/img/gallery/12.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/12.jpg" alt=""
                    /></a>
                    <a
                      href="assets/img/gallery/03.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/03.jpg" alt=""
                    /></a>
                    <a
                      href="assets/img/gallery/06.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/06.jpg" alt=""
                    /></a>
                    <a
                      href="assets/img/gallery/07.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/07.jpg" alt=""
                    /></a>
                    
                    <a
                      href="assets/img/gallery/09.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/09.jpg" alt=""
                    /></a>

                    <a
                      href="assets/img/gallery/10.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/10.jpg" alt=""
                    /></a>
                    <a
                      href="assets/img/gallery/11.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/11.jpg" alt=""
                    /></a>
                   
                    <!-- <a
                      href="assets/img/gallery/13.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/13.jpg" alt=""
                    /></a>
                    <a
                      href="assets/img/gallery/14.jpg"
                      class="item image-popup-vertical-fit"
                      ><img src="assets/img/gallery/14.jpg" alt=""
                    /></a> -->
                  </div>
                </div>

                <div id="third" class="tab-content">
                  <div class="flexbox" id="gallery">
                    <a href="#video-01" class="openVideo item">
                      <img src="assets/img/gallery/video-1-poster.png" alt="" />
                    </a>
                    <div id="video-01" class="video-popup mfp-hide">
                      <video
                        preload="false"
                        poster="assets/img/gallery/video-1-poster.png"
                      >
                        <source
                          src="assets/img/gallery/video-1.mp4"
                          type="video/mp4"
                        />
                      </video>
                    </div>
                    <a href="#video-02" class="openVideo item">
                      <img src="assets/img/gallery/video-2-poster.png" alt="" />
                    </a>
                    <div id="video-02" class="video-popup mfp-hide">
                      <video
                        preload="false"
                        poster="assets/img/gallery/video-2-poster.png"
                      >
                        <source
                          src="assets/img/gallery/video-2.mp4"
                          type="video/mp4"
                        />
                      </video>
                    </div>
                    <a href="#video-03" class="openVideo item">
                      <img src="assets/img/gallery/video-3-poster.png" alt="" />
                    </a>
                    <div id="video-03" class="video-popup mfp-hide">
                      <video
                        preload="false"
                        poster="assets/img/gallery/video-3-poster.png"
                      >
                        <source
                          src="assets/img/gallery/video-3.mp4"
                          type="video/mp4"
                        />
                      </video>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <?php @include('template-parts/footer.php') ?>
    </main>
  </body>
</html>
