
    <?php @include('template-parts/header.php') ?>

    <div
      class="overlay"
      style="background: url(assets/img/menu-bg.png) no-repeat"
    >
      <div class="sub-menu">
        <div class="LogoNav">
          <a href="index.php" class="WhiteLogo"
            ><img src="assets/img/only-white.svg" alt="Evault"
          /></a>
          <span class="Menu"><a>Close</a></span>
        </div>
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="how-it-work.php">How It Works</a></li>
          <li><a href="gallery.php">Gallery</a></li>
          <li><a href="faq.php">FAQ</a></li>
          <li><a href="who-we-are.php">Who We Are</a></li>
          <li><a href="contact-us.php">Contact</a></li>
        </ul>
      </div>
    </div>

    <main>
      <section class="HomeBanner">
        <div class="BannerContent">
          <div class="BannerText">
            <div class="homeBanner">
              <div class="delay_15">
                <h2 class="gs_reveal_heading large-headings mb1-6">
                  What is E-Vault
                </h2>
                <h4 class="GradientText gs_reveal">
                  From features to functionality.
                </h4>
                <p class="gs_reveal">
                  The E-Vault is a transparent glass safe with a digital screen
                  that displays your campaign message and a keypad that controls
                  the unlocking mechanism. Participants attempt to unlock the
                  E-Vault and claim the prize by entering the winning secret
                  code. If someone unlocks the E-Vault, the reward payout is on
                  us, for a pre-decided fee. It’s that simple.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="Video">
          <img
            src="assets/img/evault/how-it-works/image-hero.jpg"
            alt=""
            style="width: 100%; display: block;"
          />
        </div>
      </section>

      <?php @include('template-parts/CenterContentOnly.php') ?>

      <section class="left_image_right_content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6 MobileOnly">
              <div class="imgWrap">
                <img src="assets/img/evault/how-it-works/Image-2.jpg" alt="" />
              </div>
            </div>

            <div class="col-md-6">
              <div class="contentWrap text-left-md">
                <h2 class="gs_reveal_heading">The secret code</h2>
                <p class="gs_fade_reveal">
                  Before the promotion begins, a sealed security envelope
                  containing the pre-programmed winning 6-digit combination code
                  would be handed over to your team. This envelope can be used
                  to verify the winning code once entered by a participant.
                </p>
              </div>
            </div>

            <div class="col-md-6 DesktopOnly">
              <div class="imgWrap">
                <img src="assets/img/evault/how-it-works/Image-2.jpg" alt="" />
              </div>
            </div>

            <div class="col-md-6">
              <div class="imgWrap">
                <img src="assets/img/evault/how-it-works/Image-3.jpg" alt="" />
              </div>
            </div>
            <div class="col-md-6">
              <div class="contentWrap mr-5">
                <h2 class="gs_reveal_heading">The passcode device</h2>
                <p class="gs_fade_reveal">
                  Each participant gets one chance to try and enter the winning
                  secret code using the digital keypad on the E-Vault. Every
                  entry would have a corresponding response communicating the
                  success or failure in entering the right code.
                </p>
              </div>
            </div>

            <div class="col-md-6 MobileOnly">
              <div class="imgWrap">
                <img src="assets/img/evault/how-it-works/Image-4.jpg" alt="" />
              </div>
            </div>

            <div class="col-md-6">
              <div class="contentWrap text-left-md">
                <h2 class="gs_reveal_heading">Prize redemption</h2>
                <p class="gs_fade_reveal">
                  If the winning code is entered and the E-Vault is unlocked,
                  the secret envelope must be returned to 5th Consulting for
                  verification without any damage or signs of tampering,
                  failing which, the insurance would be void. Once verified, 5th
                  Consulting will pay the reward value on behalf of the client.
                </p>
              </div>
            </div>

            <div class="col-md-6 DesktopOnly">
              <div class="imgWrap">
                <img src="assets/img/evault/how-it-works/Image-4.jpg" alt="" />
              </div>
            </div>
          </div>
        </div>
      </section>

      <?php @include('template-parts/CenterContentWithThreeCards.php') ?>

      <section class="HeadingWithLink CTAblock">
        <div class="container">
          <h2 class="gs_reveal_heading large-headings">
            The future of rewards is&nbsp;here
          </h2>
          <a href="contact-us.php" class="GreyBtn"
            >Contact Us <img src="assets/img/arw-right.svg" alt=""
          /></a>
        </div>
      </section>

      <?php @include('template-parts/footer.php') ?>
    </main>
  </body>
</html>
